package transp.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import transp.core.entities.Order;
import transp.core.entities.Trailer;
import transp.core.entities.TrailerWorkshop;
import transp.core.entities.Truck;
import transp.core.services.TrailerService;
import transp.rest.tools.PaginationResult;

import java.util.List;

/*
    CTRL+K Commit
    CTRL+SHIFT+K Push

    Ściąga:
    GET	    pobieranie danych
    POST	dodawanie danych
    PUT	    edycja danych
    DELETE	usuwanie danych

    @RequestBody     Obiekt
    @RequestParam    Zmienna
    @PathVariable    Ścieżka

 */

@Controller
@RequestMapping("/trailers")
public class TrailerController {

    private TrailerService trailerService;

    @Autowired
    public TrailerController(TrailerService trailerService) {
        this.trailerService = trailerService;
    }

    @RequestMapping(method = RequestMethod.GET, headers = "dateFilter=true")
    public ResponseEntity<PaginationResult<List<Trailer>>> findTrailerByDate(@RequestParam String searchParam, @RequestParam int first, @RequestParam int last, @RequestParam String startDateParam, @RequestParam String endDateParam, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<Trailer>> paginationResult = trailerService.findByDate(searchParam, first, last, startDateParam, endDateParam, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<Trailer>>>(paginationResult, HttpStatus.OK);
    }

    @RequestMapping(value = "/free", method = RequestMethod.GET)
    public ResponseEntity<List<Trailer>> getFreeTrailer() {
        List<Trailer> trailerList = trailerService.getFreeTrailer();
        return new ResponseEntity<List<Trailer>>(trailerList, HttpStatus.OK);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<PaginationResult<List<Trailer>>> getAllTrailer(@RequestParam int first, @RequestParam int last, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<Trailer>> paginationResult = trailerService.getAllTrailer(first, last, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<Trailer>>>(paginationResult, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Trailer> createTrailer(@RequestBody Trailer sendTrailer) throws Exception {
        try {
            Trailer createdTrailer = trailerService.createTrailer(sendTrailer);
            return new ResponseEntity<Trailer>(createdTrailer, HttpStatus.CREATED);
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    @RequestMapping(value = "/{trailerId}", method = RequestMethod.GET)
    public ResponseEntity<Trailer> getTrailer(@PathVariable Long trailerId) {
        Trailer trailer = trailerService.findTrailer(trailerId);
        if (trailer != null) {
            return new ResponseEntity<Trailer>(trailer, HttpStatus.OK);
        } else {
            return new ResponseEntity<Trailer>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{trailerId}", method = RequestMethod.DELETE)
    public ResponseEntity<Trailer> deleteTrailer(@PathVariable Long trailerId) {
        Trailer trailer = trailerService.deleteTrailer(trailerId);
        if (trailer != null) {
            return new ResponseEntity<Trailer>(trailer, HttpStatus.OK);
        } else {
            return new ResponseEntity<Trailer>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{trailerId}", method = RequestMethod.PUT)
    public ResponseEntity<Trailer> updateTrailer(@RequestBody Trailer sendTrailer, @PathVariable Long trailerId) {
        Trailer trailer = trailerService.updateTrailer(trailerId, sendTrailer);
        if (trailer != null) {
            return new ResponseEntity<Trailer>(trailer, HttpStatus.OK);
        } else {
            return new ResponseEntity<Trailer>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/orders/{orderId}", method = RequestMethod.GET)
    public ResponseEntity<List<Trailer>> findTrailerByOrderId(@PathVariable Long orderId) {
        List<Trailer> trailerList = trailerService.findByOrderId(orderId);
        return new ResponseEntity<List<Trailer>>(trailerList, HttpStatus.OK);
    }

    @RequestMapping(value = "/{trailerId}/trailerWorkshops", method = RequestMethod.POST)
    public ResponseEntity<TrailerWorkshop> createTrailerWorkshop(@RequestBody TrailerWorkshop sendTrailerWorkshop, @PathVariable Long trailerId) {
        TrailerWorkshop trailerWorkshop = trailerService.createTrailerWorkshop(trailerId, sendTrailerWorkshop);
        if (trailerWorkshop != null) {
            return new ResponseEntity<TrailerWorkshop>(trailerWorkshop, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<TrailerWorkshop>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{trailerId}/trailerWorkshops", method = RequestMethod.GET, headers = "dateFilter=true")
    public ResponseEntity<PaginationResult<List<TrailerWorkshop>>> getTrailerWorkshopByTrailerId(@PathVariable Long trailerId, @RequestParam int first, @RequestParam int last, @RequestParam String startDateParam, @RequestParam String endDateParam, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<TrailerWorkshop>> paginationResult = trailerService.getTrailerWorkshopByTrailerId(trailerId, first, last, startDateParam, endDateParam, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<TrailerWorkshop>>>(paginationResult, HttpStatus.OK);
    }

    @RequestMapping(value = "/{trailerId}/orders", method = RequestMethod.GET, headers = "dateFilter=true")
    public ResponseEntity<PaginationResult<List<Order>>> getOrderByTrailerId(@PathVariable Long trailerId, @RequestParam String searchParam, @RequestParam int first, @RequestParam int last, @RequestParam String startDateParam, @RequestParam String endDateParam, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<Order>> paginationResult = trailerService.getOrderByTrailerId(trailerId, searchParam, first, last, startDateParam, endDateParam, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<Order>>>(paginationResult, HttpStatus.OK);
    }

    @RequestMapping(value = "/{trailerId}/trucks/{truckId}", method = RequestMethod.POST)
    public ResponseEntity<Truck> addTruck(@PathVariable Long trailerId, @PathVariable Long truckId) {
        Truck truck = trailerService.addTruck(trailerId, truckId);
        if (truck != null) {
            return new ResponseEntity<Truck>(truck, HttpStatus.OK);
        } else {
            return new ResponseEntity<Truck>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{trailerId}/trucks", method = RequestMethod.GET)
    public ResponseEntity<List<Truck>> getTruckByTrailerId(@PathVariable Long trailerId) {
        List<Truck> truckList = trailerService.getTruckByTrailerId(trailerId);
        if (truckList != null) {
            return new ResponseEntity<List<Truck>>(truckList, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<Truck>>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{trailerId}/trucks/{truckId}", method = RequestMethod.DELETE)
    public ResponseEntity<Truck> removeTruck(@PathVariable Long trailerId, @PathVariable Long truckId) {
        Truck truck = trailerService.removeTruck(trailerId, truckId);
        return new ResponseEntity<Truck>(truck, HttpStatus.OK);
    }

}
