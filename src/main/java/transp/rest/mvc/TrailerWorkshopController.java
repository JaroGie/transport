package transp.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import transp.core.entities.TrailerWorkshop;
import transp.core.services.TrailerWorkshopService;
import transp.rest.tools.PaginationResult;

import java.util.List;

/*
    CTRL+K Commit
    CTRL+SHIFT+K Push

    Ściąga:
    GET	    pobieranie danych
    POST	dodawanie danych
    PUT	    edycja danych
    DELETE	usuwanie danych

    @RequestBody     Obiekt
    @RequestParam    Zmienna
    @PathVariable    Ścieżka

 */

@Controller
@RequestMapping("/trailerWorkshops")
public class TrailerWorkshopController {

    private TrailerWorkshopService trailerWorkshopService;

    @Autowired
    public TrailerWorkshopController(TrailerWorkshopService trailerWorkshopService) {
        this.trailerWorkshopService = trailerWorkshopService;
    }

    @RequestMapping(method = RequestMethod.GET, headers = "dateFilter=true")
    public ResponseEntity<PaginationResult<List<TrailerWorkshop>>> findTrailerWorkshopByDate(@RequestParam String startDateParam, @RequestParam String endDateParam, @RequestParam int first, @RequestParam int last, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<TrailerWorkshop>> paginationResult = trailerWorkshopService.findByDate(startDateParam, endDateParam, first, last, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<TrailerWorkshop>>>(paginationResult, HttpStatus.OK);
    }

    @RequestMapping(value = "/{trailerWorkshopId}", method = RequestMethod.GET)
    public ResponseEntity<TrailerWorkshop> getTrailerWorkshop(@PathVariable Long trailerWorkshopId) {
        TrailerWorkshop trailerWorkshop = trailerWorkshopService.findTrailerWorkshop(trailerWorkshopId);
        if (trailerWorkshop != null) {
            return new ResponseEntity<TrailerWorkshop>(trailerWorkshop, HttpStatus.OK);
        } else {
            return new ResponseEntity<TrailerWorkshop>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{trailerWorkshopId}", method = RequestMethod.DELETE)
    public ResponseEntity<TrailerWorkshop> deleteTrailerWorkshop(@PathVariable Long trailerWorkshopId) {
        TrailerWorkshop trailerWorkshop = trailerWorkshopService.deleteTrailerWorkshop(trailerWorkshopId);
        if (trailerWorkshop != null) {
            return new ResponseEntity<TrailerWorkshop>(trailerWorkshop, HttpStatus.OK);
        } else {
            return new ResponseEntity<TrailerWorkshop>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{trailerWorkshopId}", method = RequestMethod.PUT)
    public ResponseEntity<TrailerWorkshop> updateTrailerWorkshop(@RequestBody TrailerWorkshop sendTrailerWorkshop, @PathVariable Long trailerWorkshopId) {
        TrailerWorkshop trailerWorkshop = trailerWorkshopService.updateTrailerWorkshop(trailerWorkshopId, sendTrailerWorkshop);
        if (trailerWorkshop != null) {
            return new ResponseEntity<TrailerWorkshop>(trailerWorkshop, HttpStatus.OK);
        } else {
            return new ResponseEntity<TrailerWorkshop>(HttpStatus.NOT_FOUND);
        }
    }

}
