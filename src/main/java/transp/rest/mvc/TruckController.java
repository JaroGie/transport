package transp.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import transp.core.entities.Truck;
import transp.core.entities.TruckWorkshop;
import transp.core.services.TruckService;
import transp.core.entities.Driver;
import transp.rest.tools.PaginationResult;

import java.util.List;

/*
    CTRL+K Commit
    CTRL+SHIFT+K Push

    Ściąga:
    GET	    pobieranie danych
    POST	dodawanie danych
    PUT	    edycja danych
    DELETE	usuwanie danych

    @RequestBody     Obiekt
    @RequestParam    Zmienna
    @PathVariable    Ścieżka

 */

@Controller
@RequestMapping("/trucks")
public class TruckController {

    private TruckService truckService;

    @Autowired
    public TruckController(TruckService truckService) {
        this.truckService = truckService;
    }

    @RequestMapping(method = RequestMethod.GET, headers = "dateFilter=true")
    public ResponseEntity<PaginationResult<List<Truck>>> findTruckByDate(@RequestParam String searchParam, @RequestParam int first, @RequestParam int last, @RequestParam String startDateParam, @RequestParam String endDateParam, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<Truck>> paginationResult = truckService.findByDate(searchParam, first, last, startDateParam, endDateParam, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<Truck>>>(paginationResult, HttpStatus.OK);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<PaginationResult<List<Truck>>> getAllTruck(@RequestParam int first, @RequestParam int last, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<Truck>> paginationResult = truckService.getAllTruck(first, last, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<Truck>>>(paginationResult, HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Truck> createTruck(@RequestBody Truck sendTruck) throws Exception {
        try {
            Truck createdTruck = truckService.createTruck(sendTruck);
            return new ResponseEntity<Truck>(createdTruck, HttpStatus.CREATED);
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    @RequestMapping(value = "/{truckId}", method = RequestMethod.GET)
    public ResponseEntity<Truck> getTruck(@PathVariable Long truckId) {
        Truck truck = truckService.findTruck(truckId);
        if (truck != null) {
            return new ResponseEntity<Truck>(truck, HttpStatus.OK);
        } else {
            return new ResponseEntity<Truck>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{truckId}", method = RequestMethod.DELETE)
    public ResponseEntity<Truck> deleteTruck(@PathVariable Long truckId) {
        Truck truck = truckService.deleteTruck(truckId);
        if (truck != null) {
            return new ResponseEntity<Truck>(truck, HttpStatus.OK);
        } else {
            return new ResponseEntity<Truck>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{truckId}", method = RequestMethod.PUT)
    public ResponseEntity<Truck> updateTruck(@RequestBody Truck sendTruck, @PathVariable Long truckId) {
        Truck truck = truckService.updateTruck(truckId, sendTruck);
        if (truck != null) {
            return new ResponseEntity<Truck>(truck, HttpStatus.OK);
        } else {
            return new ResponseEntity<Truck>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{truckId}/truckWorkshops", method = RequestMethod.POST)
    public ResponseEntity<TruckWorkshop> createTruckWorkshop(@RequestBody TruckWorkshop sendTruckWorkshop, @PathVariable Long truckId) {
        TruckWorkshop truckWorkshop = truckService.createTruckWorkshop(truckId, sendTruckWorkshop);
        if (truckWorkshop != null) {
            return new ResponseEntity<TruckWorkshop>(truckWorkshop, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<TruckWorkshop>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{truckId}/truckWorkshops", method = RequestMethod.GET, headers = "dateFilter=true")
    public ResponseEntity<PaginationResult<List<TruckWorkshop>>> getTruckWorkshopByTruckId(@PathVariable Long truckId, @RequestParam int first, @RequestParam int last, @RequestParam String startDateParam, @RequestParam String endDateParam, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<TruckWorkshop>> paginationResult = truckService.getTruckWorkshopByTruckId(truckId, first, last, startDateParam, endDateParam, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<TruckWorkshop>>>(paginationResult, HttpStatus.OK);
    }

    @RequestMapping(value = "/{truckId}/drivers/{driverId}", method = RequestMethod.POST)
    public ResponseEntity<Driver> addDriver(@PathVariable Long truckId, @PathVariable Long driverId) {
        Driver driver = truckService.addDriver(truckId, driverId);
        if (driver != null) {
            return new ResponseEntity<Driver>(driver, HttpStatus.OK);
        } else {
            return new ResponseEntity<Driver>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{truckId}/drivers", method = RequestMethod.GET)
    public ResponseEntity<List<Driver>> getDriverByTruckId(@PathVariable Long truckId) {
        List<Driver> driverList = truckService.getDriverByTruckId(truckId);
        if (driverList != null) {
            return new ResponseEntity<List<Driver>>(driverList, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<Driver>>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{truckId}/drivers/{driverId}", method = RequestMethod.DELETE)
    public ResponseEntity<Driver> removeDriver(@PathVariable Long truckId, @PathVariable Long driverId) {
        Driver driver = truckService.removeDriver(truckId, driverId);
        return new ResponseEntity<Driver>(driver, HttpStatus.OK);
    }

    @RequestMapping(value = "/trailers/{trailerId}", method = RequestMethod.GET)
    public ResponseEntity<List<Truck>> findTruckByTrailerId(@PathVariable Long trailerId) {
        List<Truck> truckList = truckService.findByTrailerId(trailerId);
        return new ResponseEntity<List<Truck>>(truckList, HttpStatus.OK);
    }

    @RequestMapping(value = "/free", method = RequestMethod.GET)
    public ResponseEntity<List<Truck>> getFreeTruck() {
        List<Truck> trailerList = truckService.getFreeTruck();
        return new ResponseEntity<List<Truck>>(trailerList, HttpStatus.OK);
    }

}