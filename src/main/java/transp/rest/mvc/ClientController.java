package transp.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import transp.core.entities.Client;
import transp.core.entities.Order;
import transp.core.services.ClientService;
import transp.rest.tools.PaginationResult;

import java.util.List;

/*
    CTRL+K Commit
    CTRL+SHIFT+K Push

    Ściąga:
    GET	    pobieranie danych
    POST	dodawanie danych
    PUT	    edycja danych
    DELETE	usuwanie danych

    @RequestBody     Obiekt
    @RequestParam    Zmienna
    @PathVariable    Ścieżka

 */

@Controller
@RequestMapping("/clients")
public class ClientController {

    private ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<PaginationResult<List<Client>>> findAllClient(@RequestParam String searchParam, @RequestParam int first, @RequestParam int last, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<Client>> paginationResult = clientService.findAllClient(searchParam, first, last, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<Client>>>(paginationResult, HttpStatus.OK);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<PaginationResult<List<Client>>> getAllClient(@RequestParam int first, @RequestParam int last, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<Client>> paginationResult = clientService.getAllClient(first, last, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<Client>>>(paginationResult, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Client> createClient(@RequestBody Client sendClient) throws Exception {
        try {
            Client createdClient = clientService.createClient(sendClient);
            return new ResponseEntity<Client>(createdClient, HttpStatus.CREATED);
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    @RequestMapping(value = "/{clientId}", method = RequestMethod.GET)
    public ResponseEntity<Client> getClient(@PathVariable Long clientId) {
        Client client = clientService.findClient(clientId);
        if (client != null) {
            return new ResponseEntity<Client>(client, HttpStatus.OK);
        } else {
            return new ResponseEntity<Client>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{clientId}", method = RequestMethod.DELETE)
    public ResponseEntity<Client> deleteClient(@PathVariable Long clientId) {
        Client client = clientService.deleteClient(clientId);
        if (client != null) {
            return new ResponseEntity<Client>(client, HttpStatus.OK);
        } else {
            return new ResponseEntity<Client>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{clientId}", method = RequestMethod.PUT)
    public ResponseEntity<Client> updateClient(@RequestBody Client sendClient, @PathVariable Long clientId) {
        Client client = clientService.updateClient(clientId, sendClient);
        if (client != null) {
            return new ResponseEntity<Client>(client, HttpStatus.OK);
        } else {
            return new ResponseEntity<Client>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{clientId}/orders", method = RequestMethod.POST)
    public ResponseEntity<Order> createOrder(@RequestBody Order sendOrder, @PathVariable Long clientId) {
        Order order = clientService.createOrder(clientId, sendOrder);
        if (order != null) {
            return new ResponseEntity<Order>(order, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<Order>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{clientId}/orders", method = RequestMethod.GET, headers = "dateFilter=true")
    public ResponseEntity<PaginationResult<List<Order>>> getOrderByClientId(@PathVariable Long clientId, @RequestParam String searchParam, @RequestParam int first, @RequestParam int last, @RequestParam String startDateParam, @RequestParam String endDateParam, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<Order>> paginationResult = clientService.getOrderByClientId(clientId, searchParam, first, last, startDateParam, endDateParam, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<Order>>>(paginationResult, HttpStatus.OK);
    }

}
