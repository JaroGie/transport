package transp.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import transp.core.entities.Order;
import transp.core.entities.LoadPlace;
import transp.core.entities.UnloadPlace;
import transp.core.entities.Trailer;
import transp.core.services.OrderService;
import transp.rest.tools.PaginationResult;

import java.util.List;

/*
    CTRL+K Commit
    CTRL+SHIFT+K Push

    Ściąga:
    GET	    pobieranie danych
    POST	dodawanie danych
    PUT	    edycja danych
    DELETE	usuwanie danych

    @RequestBody     Obiekt
    @RequestParam    Zmienna
    @PathVariable    Ścieżka

 */

@Controller
@RequestMapping("/orders")
public class OrderController {

    private OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @RequestMapping(method = RequestMethod.GET, headers = "dateFilter=true")
    public ResponseEntity<PaginationResult<List<Order>>> findOrderByDate(@RequestParam String searchParam, @RequestParam int first, @RequestParam int last, @RequestParam String startDateParam, @RequestParam String endDateParam, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<Order>> paginationResult = orderService.findByDate(searchParam, first, last, startDateParam, endDateParam, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<Order>>>(paginationResult, HttpStatus.OK);
    }

    @RequestMapping(value = "/{orderId}", method = RequestMethod.GET)
    public ResponseEntity<Order> getOrder(@PathVariable Long orderId) {
        Order order = orderService.findOrder(orderId);
        if (order != null) {
            return new ResponseEntity<Order>(order, HttpStatus.OK);
        } else {
            return new ResponseEntity<Order>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{orderId}", method = RequestMethod.DELETE)
    public ResponseEntity<Order> deleteOrder(@PathVariable Long orderId) {
        Order order = orderService.deleteOrder(orderId);
        if (order != null) {
            return new ResponseEntity<Order>(order, HttpStatus.OK);
        } else {
            return new ResponseEntity<Order>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{orderId}", method = RequestMethod.PUT)
    public ResponseEntity<Order> updateOrder(@PathVariable Long orderId, @RequestBody Order sendOrder) {
        Order order = orderService.updateOrder(orderId, sendOrder);
        if (order != null) {
            return new ResponseEntity<Order>(order, HttpStatus.OK);
        } else {
            return new ResponseEntity<Order>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/clients/{clientId}", method = RequestMethod.GET)
    public ResponseEntity<PaginationResult<List<Order>>> findOrderByClientId(@PathVariable Long clientId, @RequestParam int first, @RequestParam int last, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<Order>> paginationResult = orderService.findByClientId(clientId, first, last, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<Order>>>(paginationResult, HttpStatus.OK);
    }

    @RequestMapping(value = "/{orderId}/loadPlaces", method = RequestMethod.POST)
    public ResponseEntity<LoadPlace> createLoadPlace(@PathVariable Long orderId, @RequestBody LoadPlace sendLoadPlace) {
        LoadPlace loadPlace = orderService.createLoadPlace(orderId, sendLoadPlace);
        if (loadPlace != null) {
            return new ResponseEntity<LoadPlace>(loadPlace, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<LoadPlace>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{orderId}/loadPlaces", method = RequestMethod.GET)
    public ResponseEntity<List<LoadPlace>> getLoadPlaceByOrderId(@PathVariable Long orderId) {
        List<LoadPlace> loadPlaceList = orderService.getLoadPlaceByOrderId(orderId);
        if (loadPlaceList != null) {
            return new ResponseEntity<List<LoadPlace>>(loadPlaceList, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<LoadPlace>>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{orderId}/unloadPlaces", method = RequestMethod.POST)
    public ResponseEntity<UnloadPlace> createUnloadPlace(@PathVariable Long orderId, @RequestBody UnloadPlace sendUnloadPlace) {
        UnloadPlace unloadPlace = orderService.createUnloadPlace(orderId, sendUnloadPlace);
        if (unloadPlace != null) {
            return new ResponseEntity<UnloadPlace>(unloadPlace, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<UnloadPlace>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{orderId}/unloadPlaces", method = RequestMethod.GET)
    public ResponseEntity<List<UnloadPlace>> getUnloadPlaceByOrderId(@PathVariable Long orderId) {
        List<UnloadPlace> unloadPlaceList = orderService.getUnloadPlaceByOrderId(orderId);
        if (unloadPlaceList != null) {
            return new ResponseEntity<List<UnloadPlace>>(unloadPlaceList, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<UnloadPlace>>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{orderId}/trailers/{trailerId}", method = RequestMethod.POST)
    public ResponseEntity<Trailer> addTrailer(@PathVariable Long orderId, @PathVariable Long trailerId) {
        Trailer trailer = orderService.addTrailer(orderId, trailerId);
        if (trailer != null) {
            return new ResponseEntity<Trailer>(trailer, HttpStatus.OK);
        } else {
            return new ResponseEntity<Trailer>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{orderId}/trailers", method = RequestMethod.GET)
    public ResponseEntity<List<Trailer>> getTrailerByOrderId(@PathVariable Long orderId) {
        List<Trailer> trailerList = orderService.getTrailerByOrderId(orderId);
        if (trailerList != null) {
            return new ResponseEntity<List<Trailer>>(trailerList, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<Trailer>>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{orderId}/trailers/{trailerId}", method = RequestMethod.DELETE)
    public ResponseEntity<Trailer> removeTrailer(@PathVariable Long orderId, @PathVariable Long trailerId) {
        Trailer trailer = orderService.removeTrailer(orderId, trailerId);
        return new ResponseEntity<Trailer>(trailer, HttpStatus.OK);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<PaginationResult<List<Order>>> getAllOrder(@RequestParam int first, @RequestParam int last, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<Order>> paginationResult = orderService.getAllOrder(first, last, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<Order>>>(paginationResult, HttpStatus.OK);
    }

    @RequestMapping(value = "/unpaid", method = RequestMethod.GET)
    public ResponseEntity<PaginationResult<List<Order>>> getUnpaidOrder(@RequestParam int first, @RequestParam int last, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<Order>> paginationResult = orderService.getUnpaidOrder(first, last, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<Order>>>(paginationResult, HttpStatus.OK);
    }

    @RequestMapping(value = "/paid", method = RequestMethod.GET)
    public ResponseEntity<PaginationResult<List<Order>>> getPaidOrder(@RequestParam int first, @RequestParam int last, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<Order>> paginationResult = orderService.getPaidOrder(first, last, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<Order>>>(paginationResult, HttpStatus.OK);
    }

    @RequestMapping(value = "/ongoing", method = RequestMethod.GET)
    public ResponseEntity<PaginationResult<List<Order>>> getOngoingOrder(@RequestParam int first, @RequestParam int last, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<Order>> paginationResult = orderService.getOngoingOrder(first, last, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<Order>>>(paginationResult, HttpStatus.OK);
    }

    @RequestMapping(value = "/completed", method = RequestMethod.GET)
    public ResponseEntity<PaginationResult<List<Order>>> getCompletedOrder(@RequestParam int first, @RequestParam int last, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<Order>> paginationResult = orderService.getCompletedOrder(first, last, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<Order>>>(paginationResult, HttpStatus.OK);
    }

    @RequestMapping(value = "/summation", method = RequestMethod.GET)
    public ResponseEntity<PaginationResult<List<Order>>> getSummation(@RequestParam int first, @RequestParam int last) {
        PaginationResult<List<Order>> paginationResult = orderService.getSummation(first, last);
        return new ResponseEntity<PaginationResult<List<Order>>>(paginationResult, HttpStatus.OK);
    }

}
