package transp.rest.mvc;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import transp.core.entities.*;
import transp.core.services.*;
import transp.rest.tools.PaginationResult;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

@Controller
@RequestMapping(value = "/reports")
public class ReportController {

    @Autowired
    public DriverService driverService;

    @Autowired
    public TruckService truckService;

    @Autowired
    public TrailerService trailerService;

    @Autowired
    public ClientService clientService;

    @RequestMapping(value = "/drivers", method = RequestMethod.GET)
    @ResponseBody
    public Object driversReport(HttpServletResponse response, @RequestParam String startDateParam, @RequestParam String endDateParam) {
        JasperDesign design;
        JasperReport jasperReport;
        JasperPrint report = null;
        PaginationResult<List<Driver>> paginationResult = driverService.findByDate("", 0, 0, startDateParam, endDateParam, "surname", "ASC");
        List<Driver> list = paginationResult.getResult();
        Map<String, Object> params = new HashMap<String, Object>();
        JRDataSource jrDataSource = new JRBeanCollectionDataSource(list);
        try {
            design = JRXmlLoader.load(getClass().getResourceAsStream("/reports/allDrivers.jrxml"));
            jasperReport = JasperCompileManager.compileReport(design);
            report = JasperFillManager.fillReport(jasperReport, params, jrDataSource);
        } catch (JRException e) {
            e.printStackTrace();
        }

        response.setContentType("application/x-pdf");
        response.setHeader("Content-disposition", "attachment; filename=lista_kierowcow.pdf");
        try {
            final OutputStream outputStream = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(report, outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JRException e) {
            e.printStackTrace();
        }

        return null;
    }

    @RequestMapping(value = "/trucks", method = RequestMethod.GET)
    @ResponseBody
    public Object trucksReport(HttpServletResponse response, @RequestParam String startDateParam, @RequestParam String endDateParam) {
        JasperDesign design;
        JasperReport jasperReport;
        JasperPrint report = null;
        PaginationResult<List<Truck>> paginationResult = truckService.findByDate("", 0, 0, startDateParam, endDateParam, "brand", "ASC");
        List<Truck> list = paginationResult.getResult();
        Map<String, Object> params = new HashMap<String, Object>();
        JRDataSource jrDataSource = new JRBeanCollectionDataSource(list);
        try {
            design = JRXmlLoader.load(getClass().getResourceAsStream("/reports/allTrucks.jrxml"));
            jasperReport = JasperCompileManager.compileReport(design);
            report = JasperFillManager.fillReport(jasperReport, params, jrDataSource);
        } catch (JRException e) {
            e.printStackTrace();
        }

        response.setContentType("application/x-pdf");
        response.setHeader("Content-disposition", "attachment; filename=lista_ciezarowek.pdf");
        try {
            final OutputStream outputStream = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(report, outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JRException e) {
            e.printStackTrace();
        }

        return null;
    }

    @RequestMapping(value = "/trailers", method = RequestMethod.GET)
    @ResponseBody
    public Object trailersReport(HttpServletResponse response, @RequestParam String startDateParam, @RequestParam String endDateParam) {
        JasperDesign design;
        JasperReport jasperReport;
        JasperPrint report = null;
        PaginationResult<List<Trailer>> paginationResult = trailerService.findByDate("", 0, 0, startDateParam, endDateParam, "brand", "ASC");
        List<Trailer> list = paginationResult.getResult();
        Map<String, Object> params = new HashMap<String, Object>();
        JRDataSource jrDataSource = new JRBeanCollectionDataSource(list);
        try {
            design = JRXmlLoader.load(getClass().getResourceAsStream("/reports/allTrailers.jrxml"));
            jasperReport = JasperCompileManager.compileReport(design);
            report = JasperFillManager.fillReport(jasperReport, params, jrDataSource);
        } catch (JRException e) {
            e.printStackTrace();
        }

        response.setContentType("application/x-pdf");
        response.setHeader("Content-disposition", "attachment; filename=lista_naczep.pdf");
        try {
            final OutputStream outputStream = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(report, outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JRException e) {
            e.printStackTrace();
        }

        return null;
    }

    @RequestMapping(value = "/trucks/{truckId}/services", method = RequestMethod.GET)
    @ResponseBody
    public Object truckServicesReport(HttpServletResponse response, @PathVariable Long truckId) {
        JasperDesign design;
        JasperReport jasperReport;
        JasperPrint report = null;
        Truck truck = truckService.findTruck(truckId);
        PaginationResult<List<TruckWorkshop>> paginationResult = truckService.getTruckWorkshopByTruckId(truckId, 0, 0, "1970-01-01", "2100-01-01", "truckInspectionDate", "DESC");
        List<TruckWorkshop> list = paginationResult.getResult();
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("MODEL", truck.getBrand() + " " + truck.getModel() + " " + truck.getRegistrationNumber());
        JRDataSource jrDataSource = new JRBeanCollectionDataSource(list);
        try {
            design = JRXmlLoader.load(getClass().getResourceAsStream("/reports/truckServices.jrxml"));
            jasperReport = JasperCompileManager.compileReport(design);
            report = JasperFillManager.fillReport(jasperReport, params, jrDataSource);
        } catch (JRException e) {
            e.printStackTrace();
        }

        response.setContentType("application/x-pdf");
        response.setHeader("Content-disposition", "attachment; filename=serwisowanie_" + truck.getBrand() + "_" + truck.getRegistrationNumber() + ".pdf");
        try {
            final OutputStream outputStream = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(report, outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JRException e) {
            e.printStackTrace();
        }

        return null;
    }

    @RequestMapping(value = "/trailers/{trailerId}/services", method = RequestMethod.GET)
    @ResponseBody
    public Object trailerServicesReport(HttpServletResponse response, @PathVariable Long trailerId) {
        JasperDesign design;
        JasperReport jasperReport;
        JasperPrint report = null;
        Trailer trailer = trailerService.findTrailer(trailerId);
        PaginationResult<List<TrailerWorkshop>> paginationResult = trailerService.getTrailerWorkshopByTrailerId(trailerId, 0, 0, "1970-01-01", "2100-01-01", "trailerInspectionDate", "DESC");
        List<TrailerWorkshop> list = paginationResult.getResult();
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("MODEL", trailer.getBrand() + " " + trailer.getModel() + " " + trailer.getRegistrationNumber());
        JRDataSource jrDataSource = new JRBeanCollectionDataSource(list);
        try {
            design = JRXmlLoader.load(getClass().getResourceAsStream("/reports/trailerServices.jrxml"));
            jasperReport = JasperCompileManager.compileReport(design);
            report = JasperFillManager.fillReport(jasperReport, params, jrDataSource);
        } catch (JRException e) {
            e.printStackTrace();
        }

        response.setContentType("application/x-pdf");
        response.setHeader("Content-disposition", "attachment; filename=serwisowanie_" + trailer.getBrand() + "_" + trailer.getRegistrationNumber() + ".pdf");
        try {
            final OutputStream outputStream = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(report, outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JRException e) {
            e.printStackTrace();
        }

        return null;
    }


    @RequestMapping(value = "/clients/{clientId}/orders", method = RequestMethod.GET)
    @ResponseBody
    public Object clientOrdersReport(HttpServletResponse response, @PathVariable Long clientId) {
        JasperDesign design;
        JasperReport jasperReport;
        JasperPrint report = null;
        Client client = clientService.findClient(clientId);
        PaginationResult<List<Order>> paginationResult = clientService.getOrderByClientId(clientId, "", 0, 0, "1970-01-01", "2100-01-01", "orderDate", "DESC");
        List<Order> list = paginationResult.getResult();
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("MODEL", client.getName() + " " + client.getSurname() + " " + client.getCompanyName());
        JRDataSource jrDataSource = new JRBeanCollectionDataSource(list);
        try {
            design = JRXmlLoader.load(getClass().getResourceAsStream("/reports/clientOrders.jrxml"));
            jasperReport = JasperCompileManager.compileReport(design);
            report = JasperFillManager.fillReport(jasperReport, params, jrDataSource);
        } catch (JRException e) {
            e.printStackTrace();
        }

        response.setContentType("application/x-pdf");
        response.setHeader("Content-disposition", "attachment; filename=zlecenia_" + client.getCompanyName() + ".pdf");
        try {
            final OutputStream outputStream = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(report, outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JRException e) {
            e.printStackTrace();
        }

        return null;
    }

}
