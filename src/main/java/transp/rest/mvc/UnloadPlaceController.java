package transp.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import transp.core.entities.UnloadPlace;
import transp.core.services.UnloadPlaceService;
import transp.rest.tools.PaginationResult;

import java.util.List;

/*
    CTRL+K Commit
    CTRL+SHIFT+K Push

    Ściąga:
    GET	    pobieranie danych
    POST	dodawanie danych
    PUT	    edycja danych
    DELETE	usuwanie danych

    @RequestBody     Obiekt
    @RequestParam    Zmienna
    @PathVariable    Ścieżka

 */

@Controller
@RequestMapping("/unloadPlaces")
public class UnloadPlaceController {

    private UnloadPlaceService unloadPlaceService;

    @Autowired
    private UnloadPlaceController(UnloadPlaceService unloadPlaceService) {
        this.unloadPlaceService = unloadPlaceService;
    }

    @RequestMapping(method = RequestMethod.GET, headers = "dateFilter=true")
    public ResponseEntity<PaginationResult<List<UnloadPlace>>> findUnloadPlaceByDate(@RequestParam String searchParam, @RequestParam String startDateParam, @RequestParam String endDateParam, @RequestParam int first, @RequestParam int last, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<UnloadPlace>> paginationResult = unloadPlaceService.findByDate(searchParam, startDateParam, endDateParam, first, last, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<UnloadPlace>>>(paginationResult, HttpStatus.OK);
    }

    @RequestMapping(value = "/{unloadPlaceId}", method = RequestMethod.GET)
    public ResponseEntity<UnloadPlace> getUnloadPlace(@PathVariable Long unloadPlaceId) {
        UnloadPlace unloadPlace = unloadPlaceService.findUnloadPlace(unloadPlaceId);
        if (unloadPlace != null) {
            return new ResponseEntity<UnloadPlace>(unloadPlace, HttpStatus.OK);
        } else {
            return new ResponseEntity<UnloadPlace>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{unloadPlaceId}", method = RequestMethod.PUT)
    public ResponseEntity<UnloadPlace> updateUnloadPlace(@RequestBody UnloadPlace sendUnloadPlace, @PathVariable Long unloadPlaceId) {
        UnloadPlace unloadPlace = unloadPlaceService.updateUnloadPlace(unloadPlaceId, sendUnloadPlace);
        if (unloadPlace != null) {
            return new ResponseEntity<UnloadPlace>(unloadPlace, HttpStatus.OK);
        } else {
            return new ResponseEntity<UnloadPlace>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{unloadPlaceId}", method = RequestMethod.DELETE)
    public ResponseEntity<UnloadPlace> deleteUnloadPlace(@PathVariable Long unloadPlaceId) {
        UnloadPlace unloadPlace = unloadPlaceService.deleteUnloadPlace(unloadPlaceId);
        if (unloadPlace != null) {
            return new ResponseEntity<UnloadPlace>(unloadPlace, HttpStatus.OK);
        } else {
            return new ResponseEntity<UnloadPlace>(HttpStatus.NOT_FOUND);
        }
    }

}
