package transp.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import transp.core.entities.LoadPlace;
import transp.core.services.LoadPlaceService;
import transp.rest.tools.PaginationResult;

import java.util.List;

/*
    CTRL+K Commit
    CTRL+SHIFT+K Push

    Ściąga:
    GET	    pobieranie danych
    POST	dodawanie danych
    PUT	    edycja danych
    DELETE	usuwanie danych

    @RequestBody     Obiekt
    @RequestParam    Zmienna
    @PathVariable    Ścieżka

 */

@Controller
@RequestMapping("/loadPlaces")
public class LoadPlaceController {

    private LoadPlaceService loadPlaceService;

    @Autowired
    private LoadPlaceController(LoadPlaceService loadPlaceService) {
        this.loadPlaceService = loadPlaceService;
    }

    @RequestMapping(method = RequestMethod.GET, headers = "dateFilter=true")
    public ResponseEntity<PaginationResult<List<LoadPlace>>> findLoadPlaceByDate(@RequestParam String searchParam, @RequestParam String startDateParam, @RequestParam String endDateParam, @RequestParam int first, @RequestParam int last, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<LoadPlace>> paginationResult = loadPlaceService.findByDate(searchParam, startDateParam, endDateParam, first, last, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<LoadPlace>>>(paginationResult, HttpStatus.OK);
    }

    @RequestMapping(value = "/{loadPlaceId}", method = RequestMethod.GET)
    public ResponseEntity<LoadPlace> getLoadPlace(@PathVariable Long loadPlaceId) {
        LoadPlace loadPlace = loadPlaceService.findLoadPlace(loadPlaceId);
        if (loadPlace != null) {
            return new ResponseEntity<LoadPlace>(loadPlace, HttpStatus.OK);
        } else {
            return new ResponseEntity<LoadPlace>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{loadPlaceId}", method = RequestMethod.PUT)
    public ResponseEntity<LoadPlace> updateLoadPlace(@RequestBody LoadPlace sendLoadPlace, @PathVariable Long loadPlaceId) {
        LoadPlace loadPlace = loadPlaceService.updateLoadPlace(loadPlaceId, sendLoadPlace);
        if (loadPlace != null) {
            return new ResponseEntity<LoadPlace>(loadPlace, HttpStatus.OK);
        } else {
            return new ResponseEntity<LoadPlace>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{loadPlaceId}", method = RequestMethod.DELETE)
    public ResponseEntity<LoadPlace> deleteLoadPlace(@PathVariable Long loadPlaceId) {
        LoadPlace loadPlace = loadPlaceService.deleteLoadPlace(loadPlaceId);
        if (loadPlace != null) {
            return new ResponseEntity<LoadPlace>(loadPlace, HttpStatus.OK);
        } else {
            return new ResponseEntity<LoadPlace>(HttpStatus.NOT_FOUND);
        }
    }
}
