package transp.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import transp.core.entities.TruckWorkshop;
import transp.core.services.TruckWorkshopService;
import transp.rest.tools.PaginationResult;

import java.util.List;

/*
    CTRL+K Commit
    CTRL+SHIFT+K Push

    Ściąga:
    GET	    pobieranie danych
    POST	dodawanie danych
    PUT	    edycja danych
    DELETE	usuwanie danych

    @RequestBody     Obiekt
    @RequestParam    Zmienna
    @PathVariable    Ścieżka

 */

@Controller
@RequestMapping("/truckWorkshops")
public class TruckWorkshopController {

    private TruckWorkshopService truckWorkshopService;

    @Autowired
    public TruckWorkshopController(TruckWorkshopService truckWorkshopService) {
        this.truckWorkshopService = truckWorkshopService;
    }

    @RequestMapping(method = RequestMethod.GET, headers = "dateFilter=true")
    public ResponseEntity<PaginationResult<List<TruckWorkshop>>> findTrailerWorkshopByDate(@RequestParam String startDateParam, @RequestParam String endDateParam, @RequestParam int first, @RequestParam int last, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<TruckWorkshop>> paginationResult = truckWorkshopService.findByDate(startDateParam, endDateParam, first, last, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<TruckWorkshop>>>(paginationResult, HttpStatus.OK);
    }

    @RequestMapping(value = "/{truckWorkshopId}", method = RequestMethod.GET)
    public ResponseEntity<TruckWorkshop> getTruckWorkshop(@PathVariable Long truckWorkshopId) {
        TruckWorkshop truckWorkshop = truckWorkshopService.findTruckWorkshop(truckWorkshopId);
        if (truckWorkshop != null) {
            return new ResponseEntity<TruckWorkshop>(truckWorkshop, HttpStatus.OK);
        } else {
            return new ResponseEntity<TruckWorkshop>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{truckWorkshopId}", method = RequestMethod.DELETE)
    public ResponseEntity<TruckWorkshop> deleteTruckWorkShop(@PathVariable Long truckWorkshopId) {
        TruckWorkshop truckWorkshop = truckWorkshopService.deleteTruckWorkshop(truckWorkshopId);
        if (truckWorkshop != null) {
            return new ResponseEntity<TruckWorkshop>(truckWorkshop, HttpStatus.OK);
        } else {
            return new ResponseEntity<TruckWorkshop>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{truckWorkshopId}", method = RequestMethod.PUT)
    public ResponseEntity<TruckWorkshop> updateTruckWorkshop(@RequestBody TruckWorkshop sendTruckWorkshop, @PathVariable Long truckWorkshopId) {
        TruckWorkshop truckWorkshop = truckWorkshopService.updateTruckWorkshop(truckWorkshopId, sendTruckWorkshop);
        if (truckWorkshop != null) {
            return new ResponseEntity<TruckWorkshop>(truckWorkshop, HttpStatus.OK);
        } else {
            return new ResponseEntity<TruckWorkshop>(HttpStatus.NOT_FOUND);
        }
    }

}
