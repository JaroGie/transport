package transp.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import transp.core.entities.Driver;
import transp.core.services.DriverService;
import transp.rest.tools.PaginationResult;

import java.util.List;

/*
    CTRL+K Commit
    CTRL+SHIFT+K Push

    Ściąga:
    GET	    pobieranie danych
    POST	dodawanie danych
    PUT	    edycja danych
    DELETE	usuwanie danych

    @RequestBody     Obiekt
    @RequestParam    Zmienna
    @PathVariable    Ścieżka

 */

@Controller
@RequestMapping("/drivers")
public class DriverController {

    private DriverService driverService;

    @Autowired
    public DriverController(DriverService driverService) {
        this.driverService = driverService;
    }

    @RequestMapping(method = RequestMethod.GET, headers = "dateFilter=true")
    public ResponseEntity<PaginationResult<List<Driver>>> findDriverByDate(@RequestParam String searchParam, @RequestParam int first, @RequestParam int last, @RequestParam String startDateParam, @RequestParam String endDateParam, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<Driver>> paginationResult = driverService.findByDate(searchParam, first, last, startDateParam, endDateParam, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<Driver>>>(paginationResult, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<PaginationResult<List<Driver>>> findDriverByExpiryDate(@RequestParam int first, @RequestParam int last, @RequestParam String startDateParam, @RequestParam String endDateParam, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<Driver>> paginationResult = driverService.findByExpiryDate(first, last, startDateParam, endDateParam, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<Driver>>>(paginationResult, HttpStatus.OK);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<PaginationResult<List<Driver>>> getAllDriver(@RequestParam int first, @RequestParam int last, @RequestParam String sortParam, @RequestParam String sortType) {
        PaginationResult<List<Driver>> paginationResult = driverService.getAllDriver(first, last, sortParam, sortType);
        return new ResponseEntity<PaginationResult<List<Driver>>>(paginationResult, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Driver> createDriver(@RequestBody Driver sendDriver) throws Exception {
        try {
            Driver createdDriver = driverService.createDriver(sendDriver);
            return new ResponseEntity<Driver>(createdDriver, HttpStatus.CREATED);
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    @RequestMapping(value = "/{driverId}", method = RequestMethod.GET)
    public ResponseEntity<Driver> getDriver(@PathVariable Long driverId) {
        Driver driver = driverService.findDriver(driverId);
        if (driver != null) {
            return new ResponseEntity<Driver>(driver, HttpStatus.OK);
        } else {
            return new ResponseEntity<Driver>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{driverId}", method = RequestMethod.DELETE)
    public ResponseEntity<Driver> deleteDriver(@PathVariable Long driverId) {
        Driver driver = driverService.deleteDriver(driverId);
        if (driver != null) {
            return new ResponseEntity<Driver>(driver, HttpStatus.OK);
        } else {
            return new ResponseEntity<Driver>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{driverId}", method = RequestMethod.PUT)
    public ResponseEntity<Driver> updateDriver(@RequestBody Driver sendDriver, @PathVariable Long driverId) {
        Driver driver = driverService.updateDriver(driverId, sendDriver);
        if (driver != null) {
            return new ResponseEntity<Driver>(driver, HttpStatus.OK);
        } else {
            return new ResponseEntity<Driver>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/free", method = RequestMethod.GET)
    public ResponseEntity<List<Driver>> getFreeDriver() {
        List<Driver> trailerList = driverService.getFreeDriver();
        return new ResponseEntity<List<Driver>>(trailerList, HttpStatus.OK);
    }

}
