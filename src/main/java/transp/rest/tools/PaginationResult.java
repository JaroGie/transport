package transp.rest.tools;


public class PaginationResult<E> {

    private E result;

    private int total;

    public E getResult() {
        return result;
    }

    public void setResult(E result) {
        this.result = result;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
