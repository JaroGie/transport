package transp.core.services;

import transp.core.entities.UnloadPlace;
import transp.rest.tools.PaginationResult;

import java.util.List;

public interface UnloadPlaceService {

    public UnloadPlace createUnloadPlace(UnloadPlace unloadPlace);

    public UnloadPlace updateUnloadPlace(Long id, UnloadPlace unloadPlace);

    public UnloadPlace deleteUnloadPlace(Long id);

    public UnloadPlace findUnloadPlace(Long id);

    public PaginationResult<List<UnloadPlace>> findByDate(String searchParam, String startDateParam, String endDateParam, int first, int last, String sortParam, String sortType);

}
