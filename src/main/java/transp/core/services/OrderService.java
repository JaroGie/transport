package transp.core.services;

//import transp.core.entities.Client;

import transp.core.entities.Order;
import transp.core.entities.LoadPlace;
import transp.core.entities.UnloadPlace;
import transp.core.entities.Trailer;
import transp.rest.tools.PaginationResult;

import java.util.List;

public interface OrderService {

    public Order createOrder(Order order);

    public Order updateOrder(Long id, Order order);

    public Order deleteOrder(Long id);

    public Order findOrder(Long id);

    public LoadPlace createLoadPlace(Long orderId, LoadPlace data);

    public UnloadPlace createUnloadPlace(Long orderId, UnloadPlace data);

    public List<LoadPlace> getLoadPlaceByOrderId(Long orderId);

    public List<UnloadPlace> getUnloadPlaceByOrderId(Long orderId);

    public Trailer addTrailer(Long orderId, Long trailerId);

    public Trailer removeTrailer(Long orderId, Long trailerId);

    public List<Trailer> getTrailerByOrderId(Long orderId);

    public PaginationResult<List<Order>> getAllOrder(int first, int last, String sortParam, String sortType);

    public PaginationResult<List<Order>> getUnpaidOrder(int first, int last, String sortParam, String sortType);

    public PaginationResult<List<Order>> getPaidOrder(int first, int last, String sortParam, String sortType);

    public PaginationResult<List<Order>> getOngoingOrder(int first, int last, String sortParam, String sortType);

    public PaginationResult<List<Order>> getCompletedOrder(int first, int last, String sortParam, String sortType);

    public PaginationResult<List<Order>> getSummation(int first, int last);

    public PaginationResult<List<Order>> findByClientId(Long clientId, int first, int last, String sortParam, String sortType);

    public PaginationResult<List<Order>> findByDate(String searchParam, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType);

}
