package transp.core.services;

import transp.core.entities.TruckWorkshop;
import transp.rest.tools.PaginationResult;
import java.util.List;

public interface TruckWorkshopService {

    public TruckWorkshop createTruckWorkshop(TruckWorkshop truckWorkshop);

    public TruckWorkshop updateTruckWorkshop(Long id, TruckWorkshop truckWorkshop);

    public TruckWorkshop deleteTruckWorkshop(Long id);

    public TruckWorkshop findTruckWorkshop(Long id);

    public PaginationResult<List<TruckWorkshop>> findByDate(String startDateParam, String endDateParam, int first, int last, String sortParam, String sortType);

}
