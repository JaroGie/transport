package transp.core.services;

import transp.core.entities.LoadPlace;
import transp.rest.tools.PaginationResult;

import java.util.List;

public interface LoadPlaceService {

    public LoadPlace createLoadPlace(LoadPlace loadPlace);

    public LoadPlace updateLoadPlace(Long id, LoadPlace loadPlace);

    public LoadPlace deleteLoadPlace(Long id);

    public LoadPlace findLoadPlace(Long id);

    public PaginationResult<List<LoadPlace>> findByDate(String searchParam, String startDateParam, String endDateParam, int first, int last, String sortParam, String sortType);

}
