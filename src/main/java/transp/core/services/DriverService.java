package transp.core.services;


import transp.core.entities.Driver;
import transp.rest.tools.PaginationResult;

import java.util.List;

public interface DriverService {

    public Driver createDriver(Driver driver);

    public Driver updateDriver(Long id, Driver driver);

    public Driver deleteDriver(Long id);

    public Driver findDriver(Long id);

    public List<Driver> getFreeDriver();

    public PaginationResult<List<Driver>> getAllDriver(int first, int last, String sortParam, String sortType);

//    public PaginationResult<List<Driver>> findAllDrivers(String searchParam, int first, int last, String sortParam, String sortType);

    /*public List<Driver> findByTelephoneNumber(String searchParam);*/

    /*public List<Driver> findByDriverCardId(String searchParam);*/

    public PaginationResult<List<Driver>> findByDate(String searchParam, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType);

    public PaginationResult<List<Driver>> findByExpiryDate(int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType);
}
