package transp.core.services;

import transp.core.entities.TrailerWorkshop;

import java.util.List;

import transp.rest.tools.PaginationResult;

public interface TrailerWorkshopService {

    public TrailerWorkshop createTrailerWorkshop(TrailerWorkshop trailerWorkshop);

    public TrailerWorkshop updateTrailerWorkshop(Long id, TrailerWorkshop trailerWorkshop);

    public TrailerWorkshop deleteTrailerWorkshop(Long id);

    public TrailerWorkshop findTrailerWorkshop(Long id);

    public PaginationResult<List<TrailerWorkshop>> findByDate(String startDateParam, String endDateParam, int first, int last, String sortParam, String sortType);

}
