package transp.core.services;

import transp.core.entities.Driver;
import transp.core.entities.Truck;
import transp.core.entities.TruckWorkshop;
import transp.rest.tools.PaginationResult;

import java.util.List;

public interface TruckService {

    public Truck createTruck(Truck truck);

    public Truck updateTruck(Long id, Truck truck);

    public Truck deleteTruck(Long id);

    public Truck findTruck(Long id);

    public TruckWorkshop createTruckWorkshop(Long truckId, TruckWorkshop truckWorkshop);

    public PaginationResult<List<TruckWorkshop>> getTruckWorkshopByTruckId(Long truckId, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType);

    public Driver addDriver(Long truckId, Long driverId);

    public Driver removeDriver(Long truckId, Long driverId);

    public List<Driver> getDriverByTruckId(Long truckId);

    public List<Truck> findByTrailerId(Long trailerId);

    public List<Truck> getFreeTruck();

    public PaginationResult<List<Truck>> getAllTruck(int first, int last, String sortParam, String sortType);

    public PaginationResult<List<Truck>> findByDate(String searchParam, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType);

}
