package transp.core.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import transp.core.entities.TruckWorkshop;
import transp.core.repositories.TruckWorkshopRepo;
import transp.core.services.TruckWorkshopService;
import transp.rest.tools.PaginationResult;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TruckWorkshopServiceImpl implements TruckWorkshopService {

    @Autowired
    private TruckWorkshopRepo truckWorkshopRepo;

    @Override
    public TruckWorkshop createTruckWorkshop(TruckWorkshop data) {
        return truckWorkshopRepo.createTruckWorkshop(data);
    }

    @Override
    public TruckWorkshop updateTruckWorkshop(Long id, TruckWorkshop data) {
        return truckWorkshopRepo.updateTruckWorkshop(id, data);
    }

    @Override
    public TruckWorkshop deleteTruckWorkshop(Long id) {
        return truckWorkshopRepo.deleteTruckWorkshop(id);
    }

    @Override
    public TruckWorkshop findTruckWorkshop(Long id) {
        return truckWorkshopRepo.findTruckWorkshop(id);
    }

    @Override
    public PaginationResult<List<TruckWorkshop>> findByDate(String startDateParam, String endDateParam, int first, int last, String sortParam, String sortType) {
        return truckWorkshopRepo.findByDate(startDateParam, endDateParam, first, last, sortParam, sortType);
    }

}
