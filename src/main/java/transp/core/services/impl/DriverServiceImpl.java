package transp.core.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import transp.core.entities.Driver;
import transp.core.repositories.DriverRepo;
import transp.core.services.DriverService;
import transp.rest.tools.PaginationResult;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class DriverServiceImpl implements DriverService {

    @Autowired
    private DriverRepo driverRepo;


    public Driver createDriver(Driver data) {
        return driverRepo.createDriver(data);
    }

    public Driver updateDriver(Long id, Driver data) {
        return driverRepo.updateDriver(id, data);
    }

    public Driver deleteDriver(Long id) {
        return driverRepo.deleteDriver(id);
    }

    public Driver findDriver(Long id) {
        return driverRepo.findDriver(id);
    }

    public List<Driver> getFreeDriver() {
        return driverRepo.getFreeDriver();
    }

    public PaginationResult<List<Driver>> getAllDriver(int first, int last, String sortParam, String sortType) {
        return driverRepo.getAllDriver(first, last, sortParam, sortType);
    }

//    public PaginationResult<List<Driver>> findAllDrivers(String searchParam, int first, int last, String sortParam, String sortType) {
//        return driverRepo.findAllDrivers(searchParam, first, last, sortParam, sortType);
//    }

    /*public List<Driver> findByTelephoneNumber(String searchParam) {
        return driverRepo.findByTelephoneNumber(searchParam);
    }*/

    /*public List<Driver> findByDriverCardId(String searchParam) {
        return driverRepo.findByDriverCardId(searchParam);
    }*/

    public PaginationResult<List<Driver>> findByDate(String searchParam, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType) {
        return driverRepo.findByDate(searchParam, first, last, startDateParam, endDateParam, sortParam, sortType);
    }

    public PaginationResult<List<Driver>> findByExpiryDate(int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType) {
        return driverRepo.findByExpiryDate(first, last, startDateParam, endDateParam, sortParam, sortType);
    }

}
