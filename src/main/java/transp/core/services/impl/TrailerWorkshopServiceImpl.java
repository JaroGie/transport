package transp.core.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import transp.core.entities.TrailerWorkshop;
import transp.core.repositories.TrailerWorkshopRepo;
import transp.core.services.TrailerWorkshopService;
import transp.rest.tools.PaginationResult;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TrailerWorkshopServiceImpl implements TrailerWorkshopService {

    @Autowired
    private TrailerWorkshopRepo trailerWorkshopRepo;

    @Override
    public TrailerWorkshop createTrailerWorkshop(TrailerWorkshop data) {
        return trailerWorkshopRepo.createTrailerWorkshop(data);
    }

    @Override
    public TrailerWorkshop updateTrailerWorkshop(Long id, TrailerWorkshop data) {
        return trailerWorkshopRepo.updateTrailerWorkshop(id, data);
    }

    @Override
    public TrailerWorkshop deleteTrailerWorkshop(Long id) {
        return trailerWorkshopRepo.deleteTrailerWorkshop(id);
    }

    @Override
    public TrailerWorkshop findTrailerWorkshop(Long id) {
        return trailerWorkshopRepo.findTrailerWorkshop(id);
    }

    @Override
    public PaginationResult<List<TrailerWorkshop>> findByDate(String startDateParam, String endDateParam, int first, int last, String sortParam, String sortType) {
        return trailerWorkshopRepo.findByDate(startDateParam, endDateParam, first, last, sortParam, sortType);
    }

}
