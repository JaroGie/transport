package transp.core.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import transp.core.entities.Client;
import transp.core.entities.Order;
import transp.core.repositories.ClientRepo;
import transp.core.repositories.OrderRepo;
import transp.core.services.ClientService;
import transp.rest.tools.PaginationResult;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepo clientRepo;

    @Autowired
    private OrderRepo orderRepo;

    @Override
    public Client createClient(Client data) {
        return clientRepo.createClient(data);
    }

    @Override
    public Client updateClient(Long id, Client data) {
        return clientRepo.updateClient(id, data);
    }

    @Override
    public Client deleteClient(Long id) {
        return clientRepo.deleteClient(id);
    }

    @Override
    public Client findClient(Long id) {
        return clientRepo.findClient(id);
    }

    @Override
    public Order createOrder(Long clientId, Order data) {
        Client client = clientRepo.findClient(clientId);
        if (client == null) {
            return null;
        }
        Order order = orderRepo.createOrder(data);
        order.setClient(client);
        return order;
    }

    @Override
    public PaginationResult<List<Order>> getOrderByClientId(long clientId, String searchParam, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType) {
        return clientRepo.getOrderByClientId(clientId, searchParam, first, last, startDateParam, endDateParam, sortParam, sortType);
    }

    @Override
    public PaginationResult<List<Client>> getAllClient(int first, int last, String sortParam, String sortType) {
        return clientRepo.getAllClient(first, last, sortParam, sortType);
    }

    @Override
    public PaginationResult<List<Client>> findAllClient(String searchParam, int first, int last, String sortParam, String sortType) {
        return clientRepo.findAllClient(searchParam, first, last, sortParam, sortType);
    }
}
