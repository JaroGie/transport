package transp.core.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import transp.core.entities.LoadPlace;
import transp.core.services.LoadPlaceService;
import transp.core.repositories.LoadPlaceRepo;
import transp.rest.tools.PaginationResult;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class LoadPlaceServiceImpl implements LoadPlaceService {

    @Autowired
    private LoadPlaceRepo loadPlaceRepo;

    @Override
    public LoadPlace createLoadPlace(LoadPlace data) {
        return loadPlaceRepo.createLoadPlace(data);
    }

    @Override
    public LoadPlace updateLoadPlace(Long id, LoadPlace data) {
        return loadPlaceRepo.updateLoadPlace(id, data);
    }

    @Override
    public LoadPlace deleteLoadPlace(Long id) {
        return loadPlaceRepo.deleteLoadPlace(id);
    }

    @Override
    public LoadPlace findLoadPlace(Long id) {
        return loadPlaceRepo.findLoadPlace(id);
    }

    @Override
    public PaginationResult<List<LoadPlace>> findByDate(String searchParam, String startDateParam, String endDateParam, int first, int last, String sortParam, String sortType) {
        return loadPlaceRepo.findByDate(searchParam, startDateParam, endDateParam, first, last, sortParam, sortType);
    }

}
