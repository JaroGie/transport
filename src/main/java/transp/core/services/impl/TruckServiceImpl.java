package transp.core.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import transp.core.entities.Driver;
import transp.core.entities.Truck;
import transp.core.entities.TruckWorkshop;
import transp.core.repositories.TruckRepo;
import transp.core.repositories.DriverRepo;
import transp.core.repositories.TruckWorkshopRepo;
import transp.core.services.TruckService;
import transp.rest.tools.PaginationResult;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TruckServiceImpl implements TruckService {

    @Autowired
    private TruckRepo truckRepo;

    @Autowired
    private TruckWorkshopRepo truckWorkshopRepo;

    @Autowired
    private DriverRepo driverRepo;

    @Override
    public Truck createTruck(Truck data) {
        return truckRepo.createTruck(data);
    }

    @Override
    public Truck updateTruck(Long id, Truck data) {
        return truckRepo.updateTruck(id, data);
    }

    @Override
    public Truck deleteTruck(Long id) {
        return truckRepo.deleteTruck(id);
    }

    @Override
    public Truck findTruck(Long id) {
        return truckRepo.findTruck(id);
    }

    @Override
    public TruckWorkshop createTruckWorkshop(Long truckId, TruckWorkshop data) {
        Truck truck = truckRepo.findTruck(truckId);
        if (truck == null) {
            return null;
        }
        TruckWorkshop truckWorkshop = truckWorkshopRepo.createTruckWorkshop(data);
        truckWorkshop.setTruck(truck);
        return truckWorkshop;
    }

    @Override
    public PaginationResult<List<TruckWorkshop>> getTruckWorkshopByTruckId(Long truckId, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType) {
        return truckRepo.getTruckWorkshopByTruckId(truckId, first, last, startDateParam, endDateParam, sortParam, sortType);
    }

    @Override
    public Driver addDriver(Long truckId, Long driverId) {
        Truck truck = truckRepo.findTruck(truckId);
        if (truck != null) {
            Driver driver = driverRepo.findDriver(driverId);
            if (driver != null) {
                driver.setTruck(truck);
                return driver;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public Driver removeDriver(Long truckId, Long driverId) {
        Truck truck = truckRepo.findTruck(truckId);
        if (truck != null) {
            Driver driver = driverRepo.findDriver(driverId);
            if (driver != null) {
                driver.setTruck(null);
                return driver;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public List<Driver> getDriverByTruckId(Long truckId) {
        return truckRepo.getDriverByTruckId(truckId);
    }

    @Override
    public List<Truck> findByTrailerId(Long trailerId) {
        return truckRepo.findByTrailerId(trailerId);
    }

    @Override
    public List<Truck> getFreeTruck() {
        return truckRepo.getFreeTruck();
    }

    @Override
    public PaginationResult<List<Truck>> getAllTruck(int first, int last, String sortParam, String sortType) {
        return truckRepo.getAllTruck(first, last, sortParam, sortType);
    }

    @Override
    public PaginationResult<List<Truck>> findByDate(String searchParam, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType) {
        return truckRepo.findByDate(searchParam, first, last, startDateParam, endDateParam, sortParam, sortType);
    }

}
