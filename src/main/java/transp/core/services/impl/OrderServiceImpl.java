package transp.core.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//import transp.core.entities.Client;
import transp.core.entities.Order;
import transp.core.entities.LoadPlace;
import transp.core.entities.UnloadPlace;
import transp.core.entities.Trailer;
//import transp.core.repositories.ClientRepo;
import transp.core.repositories.OrderRepo;
import transp.core.repositories.LoadPlaceRepo;
import transp.core.repositories.UnloadPlaceRepo;
import transp.core.repositories.TrailerRepo;
import transp.core.services.OrderService;
import transp.rest.tools.PaginationResult;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepo orderRepo;

    // @Autowired
    // private ClientRepo clientRepo;

    @Autowired
    private LoadPlaceRepo loadPlaceRepo;

    @Autowired
    private UnloadPlaceRepo unloadPlaceRepo;

    @Autowired
    private TrailerRepo trailerRepo;

    @Override
    public Order createOrder(Order data) {
        return orderRepo.createOrder(data);
    }

    @Override
    public Order updateOrder(Long id, Order data) {
        return orderRepo.updateOrder(id, data);
    }

    @Override
    public Order deleteOrder(Long id) {
        return orderRepo.deleteOrder(id);
    }

    @Override
    public Order findOrder(Long id) {
        return orderRepo.findOrder(id);
    }

    @Override
    public LoadPlace createLoadPlace(Long orderId, LoadPlace data) {
        Order order = orderRepo.findOrder(orderId);
        if (order == null) {
            return null;
        }
        LoadPlace loadPlace = loadPlaceRepo.createLoadPlace(data);
        loadPlace.setOrder(order);
        return loadPlace;
    }

    @Override
    public UnloadPlace createUnloadPlace(Long orderId, UnloadPlace data) {
        Order order = orderRepo.findOrder(orderId);
        if (order == null) {
            return null;
        }
        UnloadPlace unloadPlace = unloadPlaceRepo.createUnloadPlace(data);
        unloadPlace.setOrder(order);
        return unloadPlace;
    }

    @Override
    public List<LoadPlace> getLoadPlaceByOrderId(Long orderId) {
        return orderRepo.getLoadPlaceByOrderId(orderId);
    }

    @Override
    public List<UnloadPlace> getUnloadPlaceByOrderId(Long orderId) {
        return orderRepo.getUnloadPlaceByOrderId(orderId);
    }

    @Override
    public Trailer addTrailer(Long orderId, Long trailerId) {
        Order order = orderRepo.findOrder(orderId);
        if (order != null) {
            Trailer trailer = trailerRepo.findTrailer(trailerId);
            if (trailer != null) {
                List<Trailer> trailerList = order.getTrailers();
                trailerList.add(trailer);
                List<Order> orderList = trailer.getOrders();
                orderList.add(order);
                return trailer;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public Trailer removeTrailer(Long orderId, Long trailerId) {
        Order order = orderRepo.findOrder(orderId);
        if (order != null) {
            Trailer trailer = trailerRepo.findTrailer(trailerId);
            if (trailer != null) {
                List<Trailer> trailerList = order.getTrailers();
                trailerList.remove(trailer);
                List<Order> orderList = trailer.getOrders();
                orderList.remove(order);
                return trailer;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public List<Trailer> getTrailerByOrderId(Long orderId) {
        return orderRepo.getTrailerByOrderId(orderId);
    }

    @Override
    public PaginationResult<List<Order>> getAllOrder(int first, int last, String sortParam, String sortType) {
        return orderRepo.getAllOrder(first, last, sortParam, sortType);
    }

    @Override
    public PaginationResult<List<Order>> getUnpaidOrder(int first, int last, String sortParam, String sortType) {
        return orderRepo.getUnpaidOrder(first, last, sortParam, sortType);
    }

    @Override
    public PaginationResult<List<Order>> getPaidOrder(int first, int last, String sortParam, String sortType) {
        return orderRepo.getPaidOrder(first, last, sortParam, sortType);
    }

    @Override
    public PaginationResult<List<Order>> getOngoingOrder(int first, int last, String sortParam, String sortType) {
        return orderRepo.getOngoingOrder(first, last, sortParam, sortType);
    }

    @Override
    public PaginationResult<List<Order>> getCompletedOrder(int first, int last, String sortParam, String sortType) {
        return orderRepo.getCompletedOrder(first, last, sortParam, sortType);
    }

    @Override
    public PaginationResult<List<Order>> getSummation(int first, int last) {
        return orderRepo.getSummation(first, last);
    }

    @Override
    public PaginationResult<List<Order>> findByClientId(Long clientId, int first, int last, String sortParam, String sortType) {
        return orderRepo.findByClientId(clientId, first, last, sortParam, sortType);
    }

    @Override
    public PaginationResult<List<Order>> findByDate(String searchParam, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType) {
        return orderRepo.findByDate(searchParam, first, last, startDateParam, endDateParam, sortParam, sortType);
    }
}
