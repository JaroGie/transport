package transp.core.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import transp.core.entities.UnloadPlace;
import transp.core.services.UnloadPlaceService;
import transp.core.repositories.UnloadPlaceRepo;
import transp.rest.tools.PaginationResult;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UnloadPlaceServiceImpl implements UnloadPlaceService {

    @Autowired
    private UnloadPlaceRepo unloadPlaceRepo;

    @Override
    public UnloadPlace createUnloadPlace(UnloadPlace data) {
        return unloadPlaceRepo.createUnloadPlace(data);
    }

    @Override
    public UnloadPlace updateUnloadPlace(Long id, UnloadPlace data) {
        return unloadPlaceRepo.updateUnloadPlace(id, data);
    }

    @Override
    public UnloadPlace deleteUnloadPlace(Long id) {
        return unloadPlaceRepo.deleteUnloadPlace(id);
    }

    @Override
    public UnloadPlace findUnloadPlace(Long id) {
        return unloadPlaceRepo.findUnloadPlace(id);
    }

    @Override
    public PaginationResult<List<UnloadPlace>> findByDate(String searchParam, String startDateParam, String endDateParam, int first, int last, String sortParam, String sortType) {
        return unloadPlaceRepo.findByDate(searchParam, startDateParam, endDateParam, first, last, sortParam, sortType);
    }

}
