package transp.core.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import transp.core.services.TrailerService;
import transp.core.entities.Trailer;
import transp.core.entities.Order;
import transp.core.entities.TrailerWorkshop;
import transp.core.entities.Truck;
import transp.core.repositories.TrailerRepo;
import transp.core.repositories.TrailerWorkshopRepo;
import transp.core.repositories.TruckRepo;
import transp.rest.tools.PaginationResult;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TrailerServiceImpl implements TrailerService {

    @Autowired
    private TrailerRepo trailerRepo;

    @Autowired
    private TrailerWorkshopRepo trailerWorkshopRepo;

    @Autowired
    private TruckRepo truckRepo;

    public Trailer createTrailer(Trailer data) {
        return trailerRepo.createTrailer(data);
    }

    public Trailer updateTrailer(Long id, Trailer data) {
        return trailerRepo.updateTrailer(id, data);
    }

    public Trailer deleteTrailer(Long id) {
        return trailerRepo.deleteTrailer(id);
    }

    public Trailer findTrailer(Long id) {
        return trailerRepo.findTrailer(id);
    }

    public List<Trailer> getFreeTrailer() {
        return trailerRepo.getFreeTrailer();
    }

    public PaginationResult<List<Trailer>> getAllTrailer(int first, int last, String sortParam, String sortType) {
        return trailerRepo.getAllTrailer(first, last, sortParam, sortType);
    }

    public TrailerWorkshop createTrailerWorkshop(Long trailerId, TrailerWorkshop data) {
        Trailer trailer = trailerRepo.findTrailer(trailerId);
        if (trailer == null) {
            return null;
        }
        TrailerWorkshop trailerWorkshop = trailerWorkshopRepo.createTrailerWorkshop(data);
        trailerWorkshop.setTrailer(trailer);
        return trailerWorkshop;
    }

    public Truck addTruck(Long trailerId, Long truckId) {
        Trailer trailer = trailerRepo.findTrailer(trailerId);
        if (trailer != null) {
            Truck truck = truckRepo.findTruck(truckId);
            if (truck != null) {
                truck.setTrailer(trailer);
                return truck;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public Truck removeTruck(Long trailerId, Long truckId) {
        Trailer trailer = trailerRepo.findTrailer(trailerId);
        if (trailer != null) {
            Truck truck = truckRepo.findTruck(truckId);
            if (truck != null) {
                truck.setTrailer(null);
                return truck;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public PaginationResult<List<TrailerWorkshop>> getTrailerWorkshopByTrailerId(Long trailerId, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType) {
        return trailerRepo.getTrailerWorkshopByTrailerId(trailerId, first, last, startDateParam, endDateParam, sortParam, sortType);
    }

    public List<Truck> getTruckByTrailerId(Long trailerId) {
        return trailerRepo.getTruckByTrailerId(trailerId);
    }

    public PaginationResult<List<Order>> getOrderByTrailerId(Long trailerId, String searchParam, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType) {
        return trailerRepo.getOrderByTrailerId(trailerId, searchParam, first, last, startDateParam, endDateParam, sortParam, sortType);
    }

    public List<Trailer> findByOrderId(Long orderId) {
        return trailerRepo.findByOrderId(orderId);
    }

    public PaginationResult<List<Trailer>> findByDate(String searchParam, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType) {
        return trailerRepo.findByDate(searchParam, first, last, startDateParam, endDateParam, sortParam, sortType);
    }

}
