package transp.core.services;

import transp.core.entities.Trailer;
import transp.core.entities.TrailerWorkshop;
import transp.core.entities.Order;
import transp.core.entities.Truck;
import transp.rest.tools.PaginationResult;

import java.util.List;

public interface TrailerService {

    public Trailer createTrailer(Trailer trailer);

    public Trailer updateTrailer(Long id, Trailer trailer);

    public Trailer deleteTrailer(Long id);

    public Trailer findTrailer(Long id);

    public List<Trailer> getFreeTrailer();

    public PaginationResult<List<Trailer>> getAllTrailer(int first, int last, String sortParam, String sortType);

    public TrailerWorkshop createTrailerWorkshop(Long trailerId, TrailerWorkshop trailerWorkshop);

    public Truck addTruck(Long trailerId, Long truckId);

    public Truck removeTruck(Long trailerId, Long truckId);

    public PaginationResult<List<TrailerWorkshop>> getTrailerWorkshopByTrailerId(Long trailerId, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType);

    public List<Truck> getTruckByTrailerId(Long trailerId);

    public PaginationResult<List<Order>> getOrderByTrailerId(Long trailerId, String searchParam, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType);

    public List<Trailer> findByOrderId(Long orderId);

    public PaginationResult<List<Trailer>> findByDate(String searchParam, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType);
}
