package transp.core.entities;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "LOAD_PLACE")
public class LoadPlace {
    @Id
    @GeneratedValue
    @Column(name = "LOAD_PLACE_ID", nullable = false)
    private Long id;

    @Column(name = "COMPANY_NAME", nullable = false)
    private String companyName;

    @Column(name = "COMPANY_TELEPHONE_NUMBER", nullable = false)
    private String companyTelephoneNumber;

    @Column(name = "COMPANY_COUNTRY", nullable = false)
    private String companyCountry;

    @Column(name = "COMPANY_ZIP_CODE", nullable = false)
    private String companyZipCode;

    @Column(name = "COMPANY_CITY", nullable = false)
    private String companyCity;

    @Column(name = "COMPANY_STREET", nullable = true)
    private String companyStreet;

    @Column(name = "COMPANY_STREET_NUMBER", nullable = false)
    private String companyStreetNumber;

    @Column(name = "LOAD_DATE", nullable = false)
    private Date date;

    @Column(name = "SENDER_RECOMMENDATIONS", nullable = true)
    private String recommendations;

    @ManyToOne
    private Order order;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyTelephoneNumber() {
        return companyTelephoneNumber;
    }

    public void setCompanyTelephoneNumber(String companyTelephoneNumber) {
        this.companyTelephoneNumber = companyTelephoneNumber;
    }

    public String getCompanyZipCode() {
        return companyZipCode;
    }

    public void setCompanyZipCode(String companyZipCode) {
        this.companyZipCode = companyZipCode;
    }

    public String getCompanyCity() {
        return companyCity;
    }

    public void setCompanyCity(String companyCity) {
        this.companyCity = companyCity;
    }

    public String getCompanyStreet() {
        return companyStreet;
    }

    public void setCompanyStreet(String companyStreet) {
        this.companyStreet = companyStreet;
    }

    public String getCompanyStreetNumber() {
        return companyStreetNumber;
    }

    public void setCompanyStreetNumber(String companyStreetNumber) {
        this.companyStreetNumber = companyStreetNumber;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getCompanyCountry() {
        return companyCountry;
    }

    public void setCompanyCountry(String companyCountry) {
        this.companyCountry = companyCountry;
    }

    public String getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(String recommendations) {
        this.recommendations = recommendations;
    }
}
