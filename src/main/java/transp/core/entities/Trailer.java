package transp.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "TRAILER")
public class Trailer {
    @Id
    @GeneratedValue
    @Column(name = "TRAILER_ID", nullable = false)
    private Long id;

    @Column(name = "BRAND", nullable = false)
    private String brand;

    @Column(name = "MODEL", nullable = false)
    private String model;

    @Column(name = "REGISTRATION_NUMBER", nullable = false)
    private String registrationNumber;

    @Column(name = "MANUFACTURING_YEAR", nullable = false)
    private Date manufacturingYear;

    @Column(name = "TYPE", nullable = false)
    private String trailerType;

    @Column(name = "ADR_CERTIFICATE", nullable = true)
    private String adrCertificate;

    // Relations
    @JsonIgnore
    @ManyToMany
    private List<Order> orders; // reference to Order class

    @JsonIgnore
    @OneToMany(mappedBy = "trailer", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<TrailerWorkshop> trailerWorkshops;

    @JsonIgnore
    @OneToMany(mappedBy = "trailer", fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private List<Truck> trucks;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public Date getManufacturingYear() {
        return manufacturingYear;
    }

    public void setManufacturingYear(Date manufacturingYear) {
        this.manufacturingYear = manufacturingYear;
    }

    public String getTrailerType() {
        return trailerType;
    }

    public void setTrailerType(String trailerType) {
        this.trailerType = trailerType;
    }

    public List<TrailerWorkshop> getTrailerWorkshops() {
        return trailerWorkshops;
    }

    public void setTrailerWorkshops(List<TrailerWorkshop> trailerWorkshops) {
        this.trailerWorkshops = trailerWorkshops;
    }

    public List<Truck> getTrucks() {
        return trucks;
    }

    public void setTrucks(List<Truck> trucks) {
        this.trucks = trucks;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public String getAdrCertificate() {
        return adrCertificate;
    }

    public void setAdrCertificate(String adrCertificate) {
        this.adrCertificate = adrCertificate;
    }
}
