package transp.core.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "TRAILER_SERVICE")
public class TrailerWorkshop {
    @Id
    @GeneratedValue
    @Column(name = "TRAILER_SERVICE_ID", nullable = false)
    private Long id;

    @Column(name = "SERVICE_TYPE", nullable = false)
    private String serviceType;

    @Column(name = "PNEUMATIC_SYSTEM", nullable = false)
    private String pneumaticSystem;

    @Column(name = "BRAKING_SYSTEM", nullable = true)
    private String brakingSystem;

    @Column(name = "BRAKING_FORCE", nullable = true)
    private String brakingForce;

    @Column(name = "SUSPENSION", nullable = true)
    private String suspension;

    @Column(name = "TIRES", nullable = true)
    private String tires;

    @Column(name = "LIGHTING", nullable = true)
    private String lighting;

    @Column(name = "OTHER_FAULTS", nullable = true)
    private String otherFaults;

    @Column(name = "TRAILER_INSPECTION_DATE", nullable = false)
    private Date trailerInspectionDate;

    @Column(name = "TRAILER_INSPECTION_EXPIRY_DATE", nullable = false)
    private Date trailerInspectionExpiryDate;

    @Column(name = "COMMENTS", nullable = true)
    private String comments;

    @Column(name = "COSTS", nullable = false)
    private Double costs;

    @ManyToOne
    private Trailer trailer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPneumaticSystem() {
        return pneumaticSystem;
    }

    public void setPneumaticSystem(String pneumaticSystem) {
        this.pneumaticSystem = pneumaticSystem;
    }

    public String getTires() {
        return tires;
    }

    public void setTires(String tires) {
        this.tires = tires;
    }

    public String getLighting() {
        return lighting;
    }

    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    public String getOtherFaults() {
        return otherFaults;
    }

    public void setOtherFaults(String otherFaults) {
        this.otherFaults = otherFaults;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Trailer getTrailer() {
        return trailer;
    }

    public void setTrailer(Trailer trailer) {
        this.trailer = trailer;
    }

    public Date getTrailerInspectionDate() {
        return trailerInspectionDate;
    }

    public void setTrailerInspectionDate(Date trailerInspectionDate) {
        this.trailerInspectionDate = trailerInspectionDate;
    }

    public Date getTrailerInspectionExpiryDate() {
        return trailerInspectionExpiryDate;
    }

    public void setTrailerInspectionExpiryDate(Date trailerInspectionExpiryDate) {
        this.trailerInspectionExpiryDate = trailerInspectionExpiryDate;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getBrakingSystem() {
        return brakingSystem;
    }

    public void setBrakingSystem(String brakingSystem) {
        this.brakingSystem = brakingSystem;
    }

    public String getBrakingForce() {
        return brakingForce;
    }

    public void setBrakingForce(String brakingForce) {
        this.brakingForce = brakingForce;
    }

    public String getSuspension() {
        return suspension;
    }

    public void setSuspension(String suspension) {
        this.suspension = suspension;
    }

    public Double getCosts() {
        return costs;
    }

    public void setCosts(Double costs) {
        this.costs = costs;
    }
}
