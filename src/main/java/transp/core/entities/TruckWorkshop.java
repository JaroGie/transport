package transp.core.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "TRUCK_SERVICE")
public class TruckWorkshop {
    @Id
    @GeneratedValue
    @Column(name = "TRUCK_SERVICE_ID", nullable = false)
    private Long id;

    @Column(name = "SERVICE_TYPE", nullable = false)
    private String serviceType;

    @Column(name = "ENGINE", nullable = false)
    private String engine;

    @Column(name = "GEARBOX", nullable = false)
    private String gearbox;

    @Column(name = "DRIVE_SYSTEM", nullable = false)
    private String driveSystem;

    @Column(name = "PNEUMATIC_SYSTEM", nullable = false)
    private String pneumaticSystem;

    @Column(name = "BRAKING_SYSTEM", nullable = true)
    private String brakingSystem;

    @Column(name = "BRAKING_FORCE", nullable = true)
    private String brakingForce;

    @Column(name = "SUSPENSION", nullable = true)
    private String suspension;

    @Column(name = "CONSUMABLE_FLUIDS", nullable = true)
    private String consumableFluids;

    @Column(name = "NOISE_LEVEL", nullable = true)
    private String noiseLevel;

    @Column(name = "EMISSIONS", nullable = true)
    private String emissions;

    @Column(name = "TIRES", nullable = true)
    private String tires;

    @Column(name = "BATTERIES", nullable = true)
    private String batteries;

    @Column(name = "LIGHTING", nullable = true)
    private String lighting;

    @Column(name = "OTHER_FAULTS", nullable = true)
    private String otherFaults;

    @Column(name = "DISTANCE_DRIVEN", nullable = false)
    private Long distanceDriven;

    @Column(name = "TRUCK_INSPECTION_DATE", nullable = false)
    private Date truckInspectionDate;

    @Column(name = "TRUCK_INSPECTION_EXPIRY_DATE", nullable = false)
    private Date truckInspectionExpiryDate;

    @Column(name = "COMMENTS", nullable = true)
    private String comments;

    @Column(name = "COSTS", nullable = false)
    private Double costs;

    @ManyToOne
    private Truck truck;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getGearbox() {
        return gearbox;
    }

    public void setGearbox(String gearbox) {
        this.gearbox = gearbox;
    }

    public String getDriveSystem() {
        return driveSystem;
    }

    public void setDriveSystem(String driveSystem) {
        this.driveSystem = driveSystem;
    }

    public String getPneumaticSystem() {
        return pneumaticSystem;
    }

    public void setPneumaticSystem(String pneumaticSystem) {
        this.pneumaticSystem = pneumaticSystem;
    }

    public String getConsumableFluids() {
        return consumableFluids;
    }

    public void setConsumableFluids(String consumableFluids) {
        this.consumableFluids = consumableFluids;
    }

    public String getTires() {
        return tires;
    }

    public void setTires(String tires) {
        this.tires = tires;
    }

    public String getBatteries() {
        return batteries;
    }

    public void setBatteries(String batteries) {
        this.batteries = batteries;
    }

    public String getLighting() {
        return lighting;
    }

    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    public String getOtherFaults() {
        return otherFaults;
    }

    public void setOtherFaults(String otherFaults) {
        this.otherFaults = otherFaults;
    }

    public Long getDistanceDriven() {
        return distanceDriven;
    }

    public void setDistanceDriven(Long distanceDriven) {
        this.distanceDriven = distanceDriven;
    }

    public Date getTruckInspectionExpiryDate() {
        return truckInspectionExpiryDate;
    }

    public void setTruckInspectionExpiryDate(Date truckInspectionExpiryDate) {
        this.truckInspectionExpiryDate = truckInspectionExpiryDate;
    }

    public Date getTruckInspectionDate() {
        return truckInspectionDate;
    }

    public void setTruckInspectionDate(Date truckInspectionDate) {
        this.truckInspectionDate = truckInspectionDate;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Truck getTruck() {
        return truck;
    }

    public void setTruck(Truck truck) {
        this.truck = truck;
    }

    public String getBrakingSystem() {
        return brakingSystem;
    }

    public void setBrakingSystem(String brakingSystem) {
        this.brakingSystem = brakingSystem;
    }

    public String getBrakingForce() {
        return brakingForce;
    }

    public void setBrakingForce(String brakingForce) {
        this.brakingForce = brakingForce;
    }

    public String getSuspension() {
        return suspension;
    }

    public void setSuspension(String suspension) {
        this.suspension = suspension;
    }

    public String getNoiseLevel() {
        return noiseLevel;
    }

    public void setNoiseLevel(String noiseLevel) {
        this.noiseLevel = noiseLevel;
    }

    public String getEmissions() {
        return emissions;
    }

    public void setEmissions(String emissions) {
        this.emissions = emissions;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public Double getCosts() {
        return costs;
    }

    public void setCosts(Double costs) {
        this.costs = costs;
    }
}
