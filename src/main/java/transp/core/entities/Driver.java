package transp.core.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table
public class Driver {
    @Id
    @GeneratedValue
    @Column(name = "DRIVER_ID", nullable = false)
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "SURNAME", nullable = false)
    private String surname;

    @Column(name = "PERSONAL_ID", nullable = false)
    private Long personalId;

    @Column(name = "DRIVER_CARD_ID", nullable = false)
    private String driverCardId;

    @Column(name = "DRIVER_LICENSE_NUMBER", nullable = false)
    private String driverLicenseNumber;

    @Column(name = "INSURANCE_POLICY_NUMBER", nullable = false)
    private String insurancePolicyNumber;

    @Column(name = "EMPLOYMENT_DATE", nullable = false)
    private Date employmentDate;

    @Column(name = "MEDICAL_EXAMINATION_EXPIRY_DATE", nullable = false)
    private Date medicalExaminationExpiryDate;

    @Column(name = "TELEPHONE_NUMBER", nullable = false)
    private String telephoneNumber;

    @Column(name = "COUNTRY", nullable = false)
    private String country;

    @Column(name = "ZIP_CODE", nullable = false)
    private String zipCode;

    @Column(name = "CITY", nullable = false)
    private String city;

    @Column(name = "STREET", nullable = true)
    private String street;

    @Column(name = "STREET_NUMBER", nullable = false)
    private String streetNumber;

    @Column(name = "ADR_CERTIFICATE", nullable = true)
    private String adrCertificate;

    // Relations
    @ManyToOne
    private Truck truck; // reference to Truck class

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getDriverCardId() {
        return driverCardId;
    }

    public void setDriverCardId(String driverCardId) {
        this.driverCardId = driverCardId;
    }

    public String getDriverLicenseNumber() {
        return driverLicenseNumber;
    }

    public void setDriverLicenseNumber(String driverLicenseNumber) {
        this.driverLicenseNumber = driverLicenseNumber;
    }

    public String getInsurancePolicyNumber() {
        return insurancePolicyNumber;
    }

    public void setInsurancePolicyNumber(String insurancePolicyNumber) {
        this.insurancePolicyNumber = insurancePolicyNumber;
    }

    public Date getEmploymentDate() {
        return employmentDate;
    }

    public void setEmploymentDate(Date employmentDate) {
        this.employmentDate = employmentDate;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public Truck getTruck() {
        return truck;
    }

    public void setTruck(Truck truck) {
        this.truck = truck;
    }

    public String getAdrCertificate() {
        return adrCertificate;
    }

    public void setAdrCertificate(String adrCertificate) {
        this.adrCertificate = adrCertificate;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Long getPersonalId() {
        return personalId;
    }

    public void setPersonalId(Long personalId) {
        this.personalId = personalId;
    }

    public Date getMedicalExaminationExpiryDate() {
        return medicalExaminationExpiryDate;
    }

    public void setMedicalExaminationExpiryDate(Date medicalExaminationExpiryDate) {
        this.medicalExaminationExpiryDate = medicalExaminationExpiryDate;
    }
}