package transp.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "TRUCK")
public class Truck {
    @Id
    @GeneratedValue
    @Column(name = "TRUCK_ID", nullable = false)
    private Long id;

    @Column(name = "BRAND", nullable = false)
    private String brand;

    @Column(name = "MODEL", nullable = false)
    private String model;

    @Column(name = "ENGINE_MODEL", nullable = true)
    private String engineModel;

    @Column(name = "REGISTRATION_NUMBER", nullable = false)
    private String registrationNumber;

    @Column(name = "VIN", nullable = false)
    private String vin;

    @Column(name = "TRUCK_ID_CARD", nullable = false)
    private String truckIdCard;

    @Column(name = "MANUFACTURING_YEAR", nullable = false)
    private Date manufacturingYear;

    @Column(name = "ENGINE_NUMBER", nullable = false)
    private String engineNumber;

    @Column(name = "CHASSIS_NUMBER", nullable = false)
    private String chassisNumber;

    // Relations
    @ManyToOne
    private Trailer trailer; // reference to Trailer class

    @JsonIgnore
    @OneToMany(mappedBy = "truck", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<TruckWorkshop> truckWorkshops;

    @JsonIgnore
    @OneToMany(mappedBy = "truck", fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private List<Driver> drivers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getEngineModel() {
        return engineModel;
    }

    public void setEngineModel(String engineModel) {
        this.engineModel = engineModel;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getTruckIdCard() {
        return truckIdCard;
    }

    public void setTruckIdCard(String truckIdCard) {
        this.truckIdCard = truckIdCard;
    }

    public Date getManufacturingYear() {
        return manufacturingYear;
    }

    public void setManufacturingYear(Date manufacturingYear) {
        this.manufacturingYear = manufacturingYear;
    }

    public String getEngineNumber() {
        return engineNumber;
    }

    public void setEngineNumber(String engineNumber) {
        this.engineNumber = engineNumber;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public Trailer getTrailer() {
        return trailer;
    }

    public void setTrailer(Trailer trailer) {
        this.trailer = trailer;
    }

    public List<TruckWorkshop> getTruckWorkshops() {
        return truckWorkshops;
    }

    public void setTruckWorkshops(List<TruckWorkshop> truckWorkshops) {
        this.truckWorkshops = truckWorkshops;
    }

    public List<Driver> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<Driver> drivers) {
        this.drivers = drivers;
    }
}
