package transp.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "ORDERS")
public class Order {
    @Id
    @GeneratedValue
    @Column(name = "ORDER_ID", nullable = false)
    private Long id;

    @Column(name = "ORDER_NUMBER", nullable = false)
    private String orderNumber;

    @Column(name = "CARGO_DESCRIPTION", nullable = false)
    private String cargoDescription;

    @Column(name = "WEIGHT", nullable = false)
    private float weight;

    @Column(name = "CAPACITY", nullable = true)
    private float capacity;

    @Column(name = "QUANTITY", nullable = false)
    private Long quantity;

    @Column(name = "TYPE_OF_PACKAGING", nullable = true)
    private String typeOfPackaging;

    @Column(name = "DISTANCE", nullable = false)
    private float distance;

    @Column(name = "ORDER_DATE", nullable = false)
    private Date orderDate;

    @Column(name = "DELIVERY_STATUS", nullable = false)
    private Boolean deliveryStatus;

    @Column(name = "DELIVERY_PRICE", nullable = false)
    private Double deliveryPrice;

    @Column(name = "ADDITIONAL_COSTS", nullable = true)
    private Double additionalCosts;

    @Column(name = "PENALTY_COSTS", nullable = true)
    private Double penaltyCosts;

    @Column(name = "RECEIVE_MONEY", nullable = false)
    private Boolean receiveMoney;

    @Column(name = "ADR_CERTIFICATE", nullable = true)
    private String adrCertificate;

    // Relations
    @ManyToOne
    private Client client; // reference to Client class

    @JsonIgnore
    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<LoadPlace> loadPlaces; // list of all cargo load places

    @JsonIgnore
    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<UnloadPlace> unloadPlaces; // list of all cargo unload places

    @JsonIgnore
    @ManyToMany(mappedBy = "orders", fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
//    @JoinTable(name = "ORDER_TRAILER", joinColumns = {@JoinColumn(name = "ORDER_ID")},
//            inverseJoinColumns = {@JoinColumn(name = "TRUCK_ID")})
    private List<Trailer> trailers; // list of all trailers for this order

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCargoDescription() {
        return cargoDescription;
    }

    public void setCargoDescription(String cargoDescription) {
        this.cargoDescription = cargoDescription;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Boolean getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(Boolean deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public Double getDeliveryPrice() {
        return deliveryPrice;
    }

    public void setDeliveryPrice(Double deliveryPrice) {
        this.deliveryPrice = deliveryPrice;
    }

    public Double getAdditionalCosts() {
        return additionalCosts;
    }

    public void setAdditionalCosts(Double additionalCosts) {
        this.additionalCosts = additionalCosts;
    }

    public Double getPenaltyCosts() {
        return penaltyCosts;
    }

    public void setPenaltyCosts(Double penaltyCosts) {
        this.penaltyCosts = penaltyCosts;
    }

    public Boolean getReceiveMoney() {
        return receiveMoney;
    }

    public void setReceiveMoney(Boolean receiveMoney) {
        this.receiveMoney = receiveMoney;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public List<LoadPlace> getLoadPlaces() {
        return loadPlaces;
    }

    public void setLoadPlaces(List<LoadPlace> loadPlaces) {
        this.loadPlaces = loadPlaces;
    }

    public List<UnloadPlace> getUnloadPlaces() {
        return unloadPlaces;
    }

    public void setUnloadPlaces(List<UnloadPlace> unloadPlaces) {
        this.unloadPlaces = unloadPlaces;
    }

    public List<Trailer> getTrailers() {
        return trailers;
    }

    public void setTrailers(List<Trailer> trailers) {
        this.trailers = trailers;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public float getCapacity() {
        return capacity;
    }

    public void setCapacity(float capacity) {
        this.capacity = capacity;
    }

    public String getTypeOfPackaging() {
        return typeOfPackaging;
    }

    public void setTypeOfPackaging(String typeOfPackaging) {
        this.typeOfPackaging = typeOfPackaging;
    }

    public String getAdrCertificate() {
        return adrCertificate;
    }

    public void setAdrCertificate(String adrCertificate) {
        this.adrCertificate = adrCertificate;
    }
}
