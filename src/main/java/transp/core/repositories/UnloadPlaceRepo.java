package transp.core.repositories;

import transp.core.entities.UnloadPlace;
import transp.rest.tools.PaginationResult;

import java.util.List;

public interface UnloadPlaceRepo {

    public UnloadPlace createUnloadPlace(UnloadPlace data);

    public UnloadPlace updateUnloadPlace(Long id, UnloadPlace data);

    public UnloadPlace deleteUnloadPlace(Long id);

    public UnloadPlace findUnloadPlace(Long id);

    public PaginationResult<List<UnloadPlace>> findByDate(String searchParam, String startDateParam, String endDateParam, int first, int last, String sortParam, String sortType);
}