package transp.core.repositories;

import transp.core.entities.TruckWorkshop;

import java.util.List;

import transp.rest.tools.PaginationResult;

public interface TruckWorkshopRepo {

    public TruckWorkshop createTruckWorkshop(TruckWorkshop data);

    public TruckWorkshop updateTruckWorkshop(Long id, TruckWorkshop data);

    public TruckWorkshop deleteTruckWorkshop(Long id);

    public TruckWorkshop findTruckWorkshop(Long id);

    public PaginationResult<List<TruckWorkshop>> findByDate(String startDateParam, String endDateParam, int first, int last, String sortParam, String sortType);

}
