package transp.core.repositories;

import transp.core.entities.*;
import transp.rest.tools.PaginationResult;

import java.util.List;

public interface TrailerRepo {

    public Trailer createTrailer(Trailer data);

    public Trailer updateTrailer(Long id, Trailer data);

    public Trailer deleteTrailer(Long id);

    public Trailer findTrailer(Long id);

    public List<Trailer> getFreeTrailer();

    public PaginationResult<List<Trailer>> getAllTrailer(int first, int last, String sortParam, String sortType);

    public PaginationResult<List<TrailerWorkshop>> getTrailerWorkshopByTrailerId(Long trailerId, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType);

    public List<Truck> getTruckByTrailerId(Long trailerId);

    public PaginationResult<List<Order>> getOrderByTrailerId(Long trailerId, String searchParam, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType);

    public List<Trailer> findByOrderId(long orderId);

    public PaginationResult<List<Trailer>> findByDate(String searchParam, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType);

}
