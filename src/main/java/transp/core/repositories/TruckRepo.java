package transp.core.repositories;

import transp.core.entities.Truck;
import transp.core.entities.TruckWorkshop;
import transp.core.entities.Driver;
import transp.rest.tools.PaginationResult;

import java.util.List;


public interface TruckRepo {

    public Truck createTruck(Truck data);

    public Truck updateTruck(Long id, Truck data);

    public Truck deleteTruck(Long id);

    public Truck findTruck(Long id);

    public PaginationResult<List<TruckWorkshop>> getTruckWorkshopByTruckId(Long truckId, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType);

    public List<Driver> getDriverByTruckId(Long truckId);

    public List<Truck> findByTrailerId(Long trailerId);

    public List<Truck> getFreeTruck();

    public PaginationResult<List<Truck>> getAllTruck(int first, int last, String sortParam, String sortType);

    public PaginationResult<List<Truck>> findByDate(String searchParam, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType);

}