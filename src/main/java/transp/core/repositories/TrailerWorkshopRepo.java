package transp.core.repositories;

import transp.core.entities.TrailerWorkshop;

import java.util.List;

import transp.rest.tools.PaginationResult;

public interface TrailerWorkshopRepo {

    public TrailerWorkshop createTrailerWorkshop(TrailerWorkshop data);

    public TrailerWorkshop updateTrailerWorkshop(Long id, TrailerWorkshop data);

    public TrailerWorkshop deleteTrailerWorkshop(Long id);

    public TrailerWorkshop findTrailerWorkshop(Long id);

    public PaginationResult<List<TrailerWorkshop>> findByDate(String startDateParam, String endDateParam, int first, int last, String sortParam, String sortType);

}
