package transp.core.repositories.jpa;

import org.springframework.stereotype.Repository;
import transp.core.entities.Truck;
import transp.core.entities.TruckWorkshop;
import transp.core.entities.Driver;
import transp.core.repositories.TruckRepo;
import transp.rest.tools.PaginationResult;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
public class JpaTruckRepo implements TruckRepo {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Truck createTruck(Truck data) {
        em.persist(data);
        return data;
    }

    @Override
    public Truck updateTruck(Long id, Truck data) {
        Truck truck = em.find(Truck.class, id);
        if (truck != null) {
            truck.setBrand(data.getBrand());
            truck.setModel(data.getModel());
            truck.setEngineModel(data.getEngineModel());
            truck.setRegistrationNumber(data.getRegistrationNumber());
            truck.setVin(data.getVin());
            truck.setTruckIdCard(data.getTruckIdCard());
            truck.setManufacturingYear(data.getManufacturingYear());
            truck.setEngineNumber(data.getEngineNumber());
            truck.setChassisNumber(data.getChassisNumber());
            return data;
        } else {
            return null;
        }
    }

    @Override
    public Truck deleteTruck(Long id) {
        Truck truck = em.find(Truck.class, id);
        if (truck != null) {
            em.remove(truck);
            return truck;
        } else {
            return null;
        }
    }

    @Override
    public Truck findTruck(Long id) {
        return em.find(Truck.class, id);
    }

    @Override
    public PaginationResult<List<TruckWorkshop>> getTruckWorkshopByTruckId(Long truckId, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        try {
            startDate = dateFormat.parse(startDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date endDate = null;
        try {
            endDate = dateFormat.parse(endDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        calendar.add(Calendar.DATE, 1); // add val = 1 to Calendar.DATE
        endDate = calendar.getTime();
        Query query = em.createQuery("SELECT w from TruckWorkshop w join w.truck t where (t.id=?1 and w.truckInspectionDate between ?2 and ?3) order by w." + sortParam + " " + sortType);
        query.setParameter(1, truckId);
        query.setParameter(2, startDate);
        query.setParameter(3, endDate);
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<TruckWorkshop> list = query.getResultList();
        PaginationResult<List<TruckWorkshop>> paginationResult = new PaginationResult<List<TruckWorkshop>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }

    @Override
    public List<Driver> getDriverByTruckId(Long truckId) {
        Query query = em.createQuery("SELECT d from Driver d join d.truck t where t.id=?1");
        query.setParameter(1, truckId);
        return query.getResultList();
    }

    @Override
    public List<Truck> findByTrailerId(Long trailerId) {
        Query query = em.createQuery("SELECT v from Truck v where v.trailer.id=?1");
        query.setParameter(1, trailerId);
        return query.getResultList();
    }

    @Override
    public List<Truck> getFreeTruck() {
        Query query = em.createQuery("SELECT t from Truck t where t.trailer is null");
        return query.getResultList();
    }

    @Override
    public PaginationResult<List<Truck>> getAllTruck(int first, int last, String sortParam, String sortType) {
        Query query = em.createQuery("SELECT t from Truck t order by t." + sortParam + " " + sortType);
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<Truck> list = query.getResultList();
        PaginationResult<List<Truck>> paginationResult = new PaginationResult<List<Truck>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }

    @Override
    public PaginationResult<List<Truck>> findByDate(String searchParam, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        try {
            startDate = dateFormat.parse(startDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date endDate = null;
        try {
            endDate = dateFormat.parse(endDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        calendar.add(Calendar.DATE, 1); // add val = 1 to Calendar.DATE
        endDate = calendar.getTime();
        Query query = em.createQuery("SELECT t from Truck t where (t.brand LIKE ?1 or t.model LIKE ?1 or t.registrationNumber LIKE ?1) and (t.manufacturingYear between ?2 and ?3) order by t." + sortParam + " " + sortType);
        query.setParameter(1, "%" + searchParam + "%");
        query.setParameter(2, startDate);
        query.setParameter(3, endDate);
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<Truck> list = query.getResultList();
        PaginationResult<List<Truck>> paginationResult = new PaginationResult<List<Truck>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }
}
