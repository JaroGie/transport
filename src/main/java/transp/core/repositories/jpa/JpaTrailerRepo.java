package transp.core.repositories.jpa;

import org.springframework.stereotype.Repository;
import transp.core.entities.Trailer;
import transp.core.entities.TrailerWorkshop;
import transp.core.entities.Truck;
import transp.core.entities.Order;
import transp.core.repositories.TrailerRepo;
import transp.rest.tools.PaginationResult;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
public class JpaTrailerRepo implements TrailerRepo {
    @PersistenceContext
    private EntityManager em;

    @Override
    public Trailer createTrailer(Trailer data) {
        em.persist(data);
        return data;
    }

    @Override
    public Trailer updateTrailer(Long id, Trailer data) {
        Trailer trailer = em.find(Trailer.class, id);
        if (trailer != null) {
            trailer.setBrand(data.getBrand());
            trailer.setModel(data.getModel());
            trailer.setRegistrationNumber(data.getRegistrationNumber());
            trailer.setManufacturingYear(data.getManufacturingYear());
            trailer.setTrailerType(data.getTrailerType());
            trailer.setAdrCertificate(data.getAdrCertificate());
            return data;
        } else {
            return null;
        }
    }

    @Override
    public Trailer deleteTrailer(Long id) {
        Trailer trailer = em.find(Trailer.class, id);
        if (trailer != null) {
            em.remove(trailer);
            return trailer;
        } else {
            return null;
        }
    }

    @Override
    public Trailer findTrailer(Long id) {
        return em.find(Trailer.class, id);
    }

    @Override
    public List<Trailer> getFreeTrailer() {
        Query query = em.createQuery("SELECT t from Trailer t where t.orders is null");
        return query.getResultList();
    }

    @Override
    public PaginationResult<List<Trailer>> getAllTrailer(int first, int last, String sortParam, String sortType) {
        Query query = em.createQuery("SELECT t from Trailer t order by t." + sortParam + " " + sortType);
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<Trailer> list = query.getResultList();
        PaginationResult<List<Trailer>> paginationResult = new PaginationResult<List<Trailer>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }

    @Override
    public PaginationResult<List<TrailerWorkshop>> getTrailerWorkshopByTrailerId(Long trailerId, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        try {
            startDate = dateFormat.parse(startDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date endDate = null;
        try {
            endDate = dateFormat.parse(endDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        calendar.add(Calendar.DATE, 1); // add val = 1 to Calendar.DATE
        endDate = calendar.getTime();
        Query query = em.createQuery("SELECT w from TrailerWorkshop w join w.trailer t where (t.id=?1 and w.trailerInspectionDate between ?2 and ?3) order by w." + sortParam + " " + sortType);
        query.setParameter(1, trailerId);
        query.setParameter(2, startDate);
        query.setParameter(3, endDate);
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<TrailerWorkshop> list = query.getResultList();
        PaginationResult<List<TrailerWorkshop>> paginationResult = new PaginationResult<List<TrailerWorkshop>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }

    @Override
    public List<Truck> getTruckByTrailerId(Long trailerId) {
        Query query = em.createQuery("SELECT v from Truck v join v.trailer t where t.id=?1"); //v - vehicle - truck, trailer = t
        query.setParameter(1, trailerId);
        return query.getResultList();
    }

    @Override
    public PaginationResult<List<Order>> getOrderByTrailerId(Long trailerId, String searchParam, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        try {
            startDate = dateFormat.parse(startDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date endDate = null;
        try {
            endDate = dateFormat.parse(endDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        calendar.add(Calendar.DATE, 1); // add val = 1 to Calendar.DATE
        endDate = calendar.getTime();
        Query query = em.createQuery("SELECT o from Order o join o.trailers t where (t.id=?1 and o.cargoDescription LIKE ?2 and o.orderDate between ?3 and ?4) order by o." + sortParam + " " + sortType);
        query.setParameter(1, trailerId);
        query.setParameter(2, "%" + searchParam + "%");
        query.setParameter(3, startDate);
        query.setParameter(4, endDate);
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<Order> list = query.getResultList();
        PaginationResult<List<Order>> paginationResult = new PaginationResult<List<Order>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }

    @Override
    public List<Trailer> findByOrderId(long orderId) {
        Query query = em.createQuery("SELECT t from Trailer t where t.orders.id=?1");
        query.setParameter(1, orderId);
        return query.getResultList();
    }

    @Override
    public PaginationResult<List<Trailer>> findByDate(String searchParam, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        try {
            startDate = dateFormat.parse(startDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date endDate = null;
        try {
            endDate = dateFormat.parse(endDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        calendar.add(Calendar.DATE, 1); // add val = 1 to Calendar.DATE
        endDate = calendar.getTime();
        Query query = em.createQuery("SELECT t from Trailer t where (t.brand LIKE ?1 or t.model LIKE ?1 or t.registrationNumber LIKE ?1) and (t.manufacturingYear between ?2 and ?3) order by t." + sortParam + " " + sortType);
        query.setParameter(1, "%" + searchParam + "%");
        query.setParameter(2, startDate);
        query.setParameter(3, endDate);
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<Trailer> list = query.getResultList();
        PaginationResult<List<Trailer>> paginationResult = new PaginationResult<List<Trailer>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }

}
