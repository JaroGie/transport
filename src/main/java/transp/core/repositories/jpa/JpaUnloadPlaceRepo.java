package transp.core.repositories.jpa;

import org.springframework.stereotype.Repository;
import transp.core.entities.UnloadPlace;
import transp.core.repositories.UnloadPlaceRepo;
import transp.rest.tools.PaginationResult;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
public class JpaUnloadPlaceRepo implements UnloadPlaceRepo {

    @PersistenceContext
    private EntityManager em;

    @Override
    public UnloadPlace createUnloadPlace(UnloadPlace data) {
        em.persist(data);
        return data;
    }

    @Override
    public UnloadPlace updateUnloadPlace(Long id, UnloadPlace data) {
        UnloadPlace unloadPlace = em.find(UnloadPlace.class, id);
        if (unloadPlace != null) {
            unloadPlace.setCompanyName(data.getCompanyName());
            unloadPlace.setCompanyTelephoneNumber(data.getCompanyTelephoneNumber());
            unloadPlace.setCompanyCountry(data.getCompanyCountry());
            unloadPlace.setCompanyZipCode(data.getCompanyZipCode());
            unloadPlace.setCompanyCity(data.getCompanyCity());
            unloadPlace.setCompanyStreet(data.getCompanyStreet());
            unloadPlace.setCompanyStreetNumber(data.getCompanyStreetNumber());
            unloadPlace.setDate(data.getDate());
            unloadPlace.setRecommendations(data.getRecommendations());
            return unloadPlace;
        } else {
            return null;
        }
    }

    @Override
    public UnloadPlace deleteUnloadPlace(Long id) {
        UnloadPlace unloadPlace = em.find(UnloadPlace.class, id);
        if (unloadPlace != null) {
            em.remove(unloadPlace);
            return unloadPlace;
        } else {
            return null;
        }
    }

    @Override
    public UnloadPlace findUnloadPlace(Long id) {
        return em.find(UnloadPlace.class, id);
    }

    @Override
    public PaginationResult<List<UnloadPlace>> findByDate(String searchParam, String startDateParam, String endDateParam, int first, int last, String sortParam, String sortType) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        try {
            startDate = dateFormat.parse(startDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date endDate = null;
        try {
            endDate = dateFormat.parse(endDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        calendar.add(Calendar.DATE, 1); // add val = 1 to Calendar.DATE
        endDate = calendar.getTime();
        Query query = em.createQuery("SELECT u from UnloadPlace u where (u.companyName LIKE ?1) and (u.date between ?2 and ?3) order by u." + sortParam + " " + sortType);
        query.setParameter(1, "%" + searchParam + "%");
        query.setParameter(2, startDate);
        query.setParameter(3, endDate);
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<UnloadPlace> list = query.getResultList();
        PaginationResult<List<UnloadPlace>> paginationResult = new PaginationResult<List<UnloadPlace>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }

}
