package transp.core.repositories.jpa;

import org.springframework.stereotype.Repository;
import transp.core.entities.LoadPlace;
import transp.core.repositories.LoadPlaceRepo;
import transp.rest.tools.PaginationResult;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
public class JpaLoadPlaceRepo implements LoadPlaceRepo {

    @PersistenceContext
    private EntityManager em;

    @Override
    public LoadPlace createLoadPlace(LoadPlace data) {
        em.persist(data);
        return data;
    }

    @Override
    public LoadPlace updateLoadPlace(Long id, LoadPlace data) {
        LoadPlace loadPlace = em.find(LoadPlace.class, id);
        if (loadPlace != null) {
            loadPlace.setCompanyName(data.getCompanyName());
            loadPlace.setCompanyTelephoneNumber(data.getCompanyTelephoneNumber());
            loadPlace.setCompanyCountry(data.getCompanyCountry());
            loadPlace.setCompanyZipCode(data.getCompanyZipCode());
            loadPlace.setCompanyCity(data.getCompanyCity());
            loadPlace.setCompanyStreet(data.getCompanyStreet());
            loadPlace.setCompanyStreetNumber(data.getCompanyStreetNumber());
            loadPlace.setDate(data.getDate());
            loadPlace.setRecommendations(data.getRecommendations());
            return loadPlace;
        } else {
            return null;
        }
    }

    @Override
    public LoadPlace deleteLoadPlace(Long id) {
        LoadPlace loadPlace = em.find(LoadPlace.class, id);
        if (loadPlace != null) {
            em.remove(loadPlace);
            return loadPlace;
        } else {
            return null;
        }
    }

    @Override
    public LoadPlace findLoadPlace(Long id) {
        return em.find(LoadPlace.class, id);
    }

    @Override
    public PaginationResult<List<LoadPlace>> findByDate(String searchParam, String startDateParam, String endDateParam, int first, int last, String sortParam, String sortType) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date startDate = null;
        try {
            startDate = dateFormat.parse(startDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date endDate = null;
        try {
            endDate = dateFormat.parse(endDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        calendar.add(Calendar.DATE, 1); // add val = 1 to Calendar.DATE
        endDate = calendar.getTime();
        Query query = em.createQuery("SELECT l from LoadPlace l where (l.companyName LIKE ?1) and (l.date between ?2 and ?3) order by l." + sortParam + " " + sortType);
        query.setParameter(1, "%" + searchParam + "%");
        query.setParameter(2, startDate);
        query.setParameter(3, endDate);
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<LoadPlace> list = query.getResultList();
        PaginationResult<List<LoadPlace>> paginationResult = new PaginationResult<List<LoadPlace>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }

}
