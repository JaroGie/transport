package transp.core.repositories.jpa;

import org.springframework.stereotype.Repository;
import transp.core.entities.Driver;
import transp.core.repositories.DriverRepo;
import transp.rest.tools.PaginationResult;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
public class JpaDriverRepo implements DriverRepo {

    @PersistenceContext
    private EntityManager em;

    public Driver createDriver(Driver data) {
        em.persist(data);
        return data;
    }

    @Override
    public Driver updateDriver(Long id, Driver data) {
        Driver driver = em.find(Driver.class, id);
        if (driver != null) {
            driver.setName(data.getName());
            driver.setSurname(data.getSurname());
            driver.setPersonalId(data.getPersonalId());
            driver.setDriverCardId(data.getDriverCardId());
            driver.setDriverLicenseNumber(data.getDriverLicenseNumber());
            driver.setInsurancePolicyNumber(data.getInsurancePolicyNumber());
            driver.setEmploymentDate(data.getEmploymentDate());
            driver.setMedicalExaminationExpiryDate(data.getMedicalExaminationExpiryDate());
            driver.setTelephoneNumber(data.getTelephoneNumber());
            driver.setCountry(data.getCountry());
            driver.setZipCode(data.getZipCode());
            driver.setCity(data.getCity());
            driver.setStreet(data.getStreet());
            driver.setStreetNumber(data.getStreetNumber());
            driver.setAdrCertificate(data.getAdrCertificate());
            return driver;
        } else {
            return null;
        }
    }

    @Override
    public Driver deleteDriver(Long id) {
        Driver driver = em.find(Driver.class, id);
        if (driver != null) {
            em.remove(driver);
            return driver;
        } else {
            return null;
        }
    }

    @Override
    public Driver findDriver(Long id) {
        return em.find(Driver.class, id);
    }

    @Override
    public PaginationResult<List<Driver>> findByDate(String searchParam, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        try {
            startDate = dateFormat.parse(startDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date endDate = null;
        try {
            endDate = dateFormat.parse(endDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        calendar.add(Calendar.DATE, 1); // add val = 1 to Calendar.DATE
        endDate = calendar.getTime();
        String queryString = "SELECT d from Driver d where (d.name LIKE ?1 or d.surname LIKE ?1 or d.city LIKE ?1 or d.driverCardId LIKE ?1) and (d.employmentDate between ?2 and ?3) order by d." + sortParam + " " + sortType;
        Query query = em.createQuery(queryString);
        query.setParameter(1, "%" + searchParam + "%");
        query.setParameter(2, startDate);
        query.setParameter(3, endDate);
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<Driver> list = query.getResultList();
        PaginationResult<List<Driver>> paginationResult = new PaginationResult<List<Driver>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }

    @Override
    public List<Driver> getFreeDriver() {
        Query query = em.createQuery("SELECT d from Driver d where d.truck is null");
        return query.getResultList();
    }

    @Override
    public PaginationResult<List<Driver>> getAllDriver(int first, int last, String sortParam, String sortType) {
        Query query = em.createQuery("SELECT d from Driver d order by d." + sortParam + " " + sortType);
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<Driver> list = query.getResultList();
        PaginationResult<List<Driver>> paginationResult = new PaginationResult<List<Driver>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }

    @Override
    public PaginationResult<List<Driver>> findByExpiryDate(int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        try {
            startDate = dateFormat.parse(startDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date endDate = null;
        try {
            endDate = dateFormat.parse(endDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        calendar.add(Calendar.DATE, 1); // add val = 1 to Calendar.DATE
        endDate = calendar.getTime();
        Query query = em.createQuery("SELECT d from Driver d where d.medicalExaminationExpiryDate between ?1 and ?2 order by d." + sortParam + " " + sortType);
        query.setParameter(1, startDate);
        query.setParameter(2, endDate);
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<Driver> list = query.getResultList();
        PaginationResult<List<Driver>> paginationResult = new PaginationResult<List<Driver>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }

}
