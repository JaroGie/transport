package transp.core.repositories.jpa;

import org.springframework.stereotype.Repository;
import transp.core.entities.Order;
import transp.core.repositories.OrderRepo;
import transp.core.entities.LoadPlace;
import transp.core.entities.UnloadPlace;
import transp.core.entities.Trailer;
import transp.rest.tools.PaginationResult;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
public class JpaOrderRepo implements OrderRepo {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Order createOrder(Order data) {
        em.persist(data);
        return data;
    }

    @Override
    public Order updateOrder(Long id, Order data) {
        Order order = em.find(Order.class, id);
        if (order != null) {
            order.setOrderNumber(data.getOrderNumber());
            order.setCargoDescription(data.getCargoDescription());
            order.setWeight(data.getWeight());
            order.setCapacity(data.getCapacity());
            order.setQuantity(data.getQuantity());
            order.setTypeOfPackaging(data.getTypeOfPackaging());
            order.setDistance(data.getDistance());
            order.setOrderDate(data.getOrderDate());
            order.setDeliveryStatus(data.getDeliveryStatus());
            order.setDeliveryPrice(data.getDeliveryPrice());
            order.setAdditionalCosts(data.getAdditionalCosts());
            order.setPenaltyCosts(data.getPenaltyCosts());
            order.setReceiveMoney(data.getReceiveMoney());
            order.setAdrCertificate(data.getAdrCertificate());
            return order;
        } else {
            return null;
        }
    }

    @Override
    public Order deleteOrder(Long id) {
        Order order = em.find(Order.class, id);
        if (order != null) {
            em.remove(order);
            return order;
        } else {
            return null;
        }
    }

    @Override
    public Order findOrder(Long id) {
        return em.find(Order.class, id);
    }

    @Override // CLIENT ID SEARCH
    public PaginationResult<List<Order>> findByClientId(Long clientId, int first, int last, String sortParam, String sortType) {
        Query query = em.createQuery("SELECT o from Order o where o.client.id=?1 order by o." + sortParam + " " + sortType);
        query.setParameter(1, clientId);
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<Order> list = query.getResultList();
        PaginationResult<List<Order>> paginationResult = new PaginationResult<List<Order>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }

    @Override // DATE AND CARGO DESCRIPTION SEARCH
    public PaginationResult<List<Order>> findByDate(String searchParam, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        try {
            startDate = dateFormat.parse(startDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date endDate = null;
        try {
            endDate = dateFormat.parse(endDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        calendar.add(Calendar.DATE, 1); // add val = 1 to Calendar.DATE
        endDate = calendar.getTime();
        Query query = em.createQuery("SELECT o from Order o where (o.cargoDescription LIKE ?1) and (o.orderDate between ?2 and ?3) order by o." + sortParam + " " + sortType);
        query.setParameter(1, "%" + searchParam + "%");
        query.setParameter(2, startDate);
        query.setParameter(3, endDate);
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<Order> list = query.getResultList();
        PaginationResult<List<Order>> paginationResult = new PaginationResult<List<Order>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }

    @Override
    public List<LoadPlace> getLoadPlaceByOrderId(Long orderId) {
        Query query = em.createQuery("SELECT l from LoadPlace l join l.order o where o.id=?1 order by l.date DESC");
        query.setParameter(1, orderId);
        return query.getResultList();
    }

    @Override
    public List<UnloadPlace> getUnloadPlaceByOrderId(Long orderId) {
        Query query = em.createQuery("SELECT u from UnloadPlace u join u.order o where o.id=?1 order by u.date DESC");
        query.setParameter(1, orderId);
        return query.getResultList();
    }

    @Override
    public List<Trailer> getTrailerByOrderId(Long orderId) {
        Query query = em.createQuery("SELECT t from Trailer t join t.orders o where o.id=?1 order by t.brand ASC");
        query.setParameter(1, orderId);
        return query.getResultList();
    }

    @Override
    public PaginationResult<List<Order>> getAllOrder(int first, int last, String sortParam, String sortType) {
        Query query = em.createQuery("SELECT o from Order o order by o." + sortParam + " " + sortType);
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<Order> list = query.getResultList();
        PaginationResult<List<Order>> paginationResult = new PaginationResult<List<Order>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }

    @Override
    public PaginationResult<List<Order>> getUnpaidOrder(int first, int last, String sortParam, String sortType) {
        Query query = em.createQuery("SELECT o from Order o where o.receiveMoney is FALSE order by o." + sortParam + " " + sortType);
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<Order> list = query.getResultList();
        PaginationResult<List<Order>> paginationResult = new PaginationResult<List<Order>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }

    @Override
    public PaginationResult<List<Order>> getPaidOrder(int first, int last, String sortParam, String sortType) {
        Query query = em.createQuery("SELECT o from Order o where o.receiveMoney is TRUE order by o." + sortParam + " " + sortType);
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<Order> list = query.getResultList();
        PaginationResult<List<Order>> paginationResult = new PaginationResult<List<Order>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }

    @Override
    public PaginationResult<List<Order>> getOngoingOrder(int first, int last, String sortParam, String sortType) {
        Query query = em.createQuery("SELECT o from Order o where o.deliveryStatus is FALSE order by o." + sortParam + " " + sortType);
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<Order> list = query.getResultList();
        PaginationResult<List<Order>> paginationResult = new PaginationResult<List<Order>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }

    @Override
    public PaginationResult<List<Order>> getCompletedOrder(int first, int last, String sortParam, String sortType) {
        Query query = em.createQuery("SELECT o from Order o where o.deliveryStatus is TRUE order by o." + sortParam + " " + sortType);
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<Order> list = query.getResultList();
        PaginationResult<List<Order>> paginationResult = new PaginationResult<List<Order>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }

    @Override
    public PaginationResult<List<Order>> getSummation(int first, int last) {
        Query query = em.createQuery("SELECT (sum(o.deliveryPrice) + sum(o.additionalCosts) - sum(o.penaltyCosts)) from Order o group by o.client.id");
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<Order> list = query.getResultList();
        PaginationResult<List<Order>> paginationResult = new PaginationResult<List<Order>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }

}
