package transp.core.repositories.jpa;

import org.springframework.stereotype.Repository;
import transp.core.entities.Client;
import transp.core.repositories.ClientRepo;
import transp.core.entities.Order;
import transp.rest.tools.PaginationResult;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
public class JpaClientRepo implements ClientRepo {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Client createClient(Client data) {
        em.persist(data);
        return data;
    }

    @Override
    public Client updateClient(Long id, Client data) {
        Client client = em.find(Client.class, id);
        if (client != null) {
            client.setName(data.getName());
            client.setSurname(data.getSurname());
            client.setCompanyName(data.getCompanyName());
            client.setTelephoneNumber(data.getTelephoneNumber());
            client.setFax(data.getFax());
            client.setCountry(data.getCountry());
            client.setZipCode(data.getZipCode());
            client.setCity(data.getCity());
            client.setStreet(data.getStreet());
            client.setStreetNumber(data.getStreetNumber());
            client.setEmailAddress(data.getEmailAddress());
            client.setWebsite(data.getWebsite());
            return client;
        } else {
            return null;
        }
    }

    @Override
    public Client deleteClient(Long id) {
        Client client = em.find(Client.class, id);
        if (client != null) {
            em.remove(client);
            return client;
        } else {
            return null;
        }
    }

    @Override
    public Client findClient(Long id) {
        return em.find(Client.class, id);
    }

    @Override
    public PaginationResult<List<Order>> getOrderByClientId(long clientId, String searchParam, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        try {
            startDate = dateFormat.parse(startDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date endDate = null;
        try {
            endDate = dateFormat.parse(endDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        calendar.add(Calendar.DATE, 1); // add val = 1 to Calendar.DATE
        endDate = calendar.getTime();
        Query query = em.createQuery("SELECT o from Order o join o.client c where (c.id=?1 and o.cargoDescription LIKE ?2 and o.orderDate between ?3 and ?4) order by o." + sortParam + " " + sortType);
        query.setParameter(1, clientId);
        query.setParameter(2, "%" + searchParam + "%");
        query.setParameter(3, startDate);
        query.setParameter(4, endDate);
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<Order> list = query.getResultList();
        PaginationResult<List<Order>> paginationResult = new PaginationResult<List<Order>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }

    @Override
    public PaginationResult<List<Client>> getAllClient(int first, int last, String sortParam, String sortType) {
        Query query = em.createQuery("SELECT c from Client c order by c." + sortParam + " " + sortType);
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<Client> list = query.getResultList();
        PaginationResult<List<Client>> paginationResult = new PaginationResult<List<Client>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }

    @Override
    public PaginationResult<List<Client>> findAllClient(String searchParam, int first, int last, String sortParam, String sortType) {
        String queryString = "SELECT c from Client c where (c.name LIKE ?1) or (c.surname LIKE ?1) or (c.companyName LIKE ?1) order by c." + sortParam + " " + sortType;
        Query query = em.createQuery(queryString);
        query.setParameter(1, "%" + searchParam + "%");
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<Client> list = query.getResultList();
        PaginationResult<List<Client>> paginationResult = new PaginationResult<List<Client>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }
}
