package transp.core.repositories.jpa;

import org.springframework.stereotype.Repository;
import transp.core.entities.TruckWorkshop;
import transp.core.repositories.TruckWorkshopRepo;
import transp.rest.tools.PaginationResult;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
public class JpaTruckWorkshopRepo implements TruckWorkshopRepo {

    @PersistenceContext
    private EntityManager em;

    @Override
    public TruckWorkshop createTruckWorkshop(TruckWorkshop data) {
        em.persist(data);
        return data;
    }

    @Override
    public TruckWorkshop updateTruckWorkshop(Long id, TruckWorkshop data) {
        TruckWorkshop truckWorkshop = em.find(TruckWorkshop.class, id);
        if (truckWorkshop != null) {
            truckWorkshop.setServiceType(data.getServiceType());
            truckWorkshop.setEngine(data.getEngine());
            truckWorkshop.setGearbox(data.getGearbox());
            truckWorkshop.setDistanceDriven(data.getDistanceDriven());
            truckWorkshop.setDriveSystem(data.getDriveSystem());
            truckWorkshop.setPneumaticSystem(data.getPneumaticSystem());
            truckWorkshop.setBrakingSystem(data.getBrakingSystem());
            truckWorkshop.setBrakingForce(data.getBrakingForce());
            truckWorkshop.setSuspension(data.getSuspension());
            truckWorkshop.setTires(data.getTires());
            truckWorkshop.setConsumableFluids(data.getConsumableFluids());
            truckWorkshop.setNoiseLevel(data.getNoiseLevel());
            truckWorkshop.setEmissions(data.getEmissions());
            truckWorkshop.setBatteries(data.getBatteries());
            truckWorkshop.setLighting(data.getLighting());
            truckWorkshop.setOtherFaults(data.getOtherFaults());
            truckWorkshop.setTruckInspectionDate(data.getTruckInspectionDate());
            truckWorkshop.setTruckInspectionExpiryDate(data.getTruckInspectionExpiryDate());
            truckWorkshop.setComments(data.getComments());
            truckWorkshop.setCosts(data.getCosts());
            return truckWorkshop;
        } else {
            return null;
        }
    }

    @Override
    public TruckWorkshop deleteTruckWorkshop(Long id) {
        TruckWorkshop truckWorkshop = em.find(TruckWorkshop.class, id);
        if (truckWorkshop != null) {
            em.remove(truckWorkshop);
            return truckWorkshop;
        } else {
            return null;
        }
    }

    @Override
    public TruckWorkshop findTruckWorkshop(Long id) {
        return em.find(TruckWorkshop.class, id);
    }

    @Override
    public PaginationResult<List<TruckWorkshop>> findByDate(String startDateParam, String endDateParam, int first, int last, String sortParam, String sortType) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        try {
            startDate = dateFormat.parse(startDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date endDate = null;
        try {
            endDate = dateFormat.parse(endDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        calendar.add(Calendar.DATE, 1); // add val = 1 to Calendar.DATE
        endDate = calendar.getTime();
        Query query = em.createQuery("SELECT w from TruckWorkshop w where w.truckInspectionExpiryDate between ?1 and ?2 order by w." + sortParam + " " + sortType);
        query.setParameter(1, startDate);
        query.setParameter(2, endDate);
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<TruckWorkshop> list = query.getResultList();
        PaginationResult<List<TruckWorkshop>> paginationResult = new PaginationResult<List<TruckWorkshop>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }

}
