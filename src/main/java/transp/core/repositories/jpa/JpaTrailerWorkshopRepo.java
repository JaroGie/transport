package transp.core.repositories.jpa;

import org.springframework.stereotype.Repository;
import transp.core.entities.TrailerWorkshop;
import transp.core.repositories.TrailerWorkshopRepo;
import transp.rest.tools.PaginationResult;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
public class JpaTrailerWorkshopRepo implements TrailerWorkshopRepo {

    @PersistenceContext
    private EntityManager em;

    @Override
    public TrailerWorkshop createTrailerWorkshop(TrailerWorkshop data) {
        em.persist(data);
        return data;
    }

    @Override
    public TrailerWorkshop updateTrailerWorkshop(Long id, TrailerWorkshop data) {
        TrailerWorkshop trailerWorkshop = em.find(TrailerWorkshop.class, id);
        if (trailerWorkshop != null) {
            trailerWorkshop.setServiceType(data.getServiceType());
            trailerWorkshop.setPneumaticSystem(data.getPneumaticSystem());
            trailerWorkshop.setBrakingSystem(data.getBrakingSystem());
            trailerWorkshop.setBrakingForce(data.getBrakingForce());
            trailerWorkshop.setSuspension(data.getSuspension());
            trailerWorkshop.setTires(data.getTires());
            trailerWorkshop.setLighting(data.getLighting());
            trailerWorkshop.setOtherFaults(data.getOtherFaults());
            trailerWorkshop.setTrailerInspectionDate(data.getTrailerInspectionDate());
            trailerWorkshop.setTrailerInspectionExpiryDate(data.getTrailerInspectionExpiryDate());
            trailerWorkshop.setComments(data.getComments());
            trailerWorkshop.setCosts(data.getCosts());
            return trailerWorkshop;
        } else {
            return null;
        }
    }

    @Override
    public TrailerWorkshop deleteTrailerWorkshop(Long id) {
        TrailerWorkshop trailerWorkshop = em.find(TrailerWorkshop.class, id);
        if (trailerWorkshop != null) {
            em.remove(trailerWorkshop);
            return trailerWorkshop;
        } else {
            return null;
        }
    }

    @Override
    public TrailerWorkshop findTrailerWorkshop(Long id) {
        return em.find(TrailerWorkshop.class, id);
    }

    @Override
    public PaginationResult<List<TrailerWorkshop>> findByDate(String startDateParam, String endDateParam, int first, int last, String sortParam, String sortType) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        try {
            startDate = dateFormat.parse(startDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date endDate = null;
        try {
            endDate = dateFormat.parse(endDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        calendar.add(Calendar.DATE, 1); // add val = 1 to Calendar.DATE
        endDate = calendar.getTime();
        Query query = em.createQuery("SELECT w from TrailerWorkshop w where w.trailerInspectionExpiryDate between ?1 and ?2 order by w." + sortParam + " " + sortType);
        query.setParameter(1, startDate);
        query.setParameter(2, endDate);
        int total = query.getResultList().size();
        query.setFirstResult(first);
        query.setMaxResults(last);
        List<TrailerWorkshop> list = query.getResultList();
        PaginationResult<List<TrailerWorkshop>> paginationResult = new PaginationResult<List<TrailerWorkshop>>();
        paginationResult.setResult(list);
        paginationResult.setTotal(total);
        return paginationResult;
    }
}
