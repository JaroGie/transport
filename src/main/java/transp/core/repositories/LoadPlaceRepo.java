package transp.core.repositories;

import transp.core.entities.LoadPlace;
import transp.rest.tools.PaginationResult;

import java.util.List;

public interface LoadPlaceRepo {

    public LoadPlace createLoadPlace(LoadPlace data);

    public LoadPlace updateLoadPlace(Long id, LoadPlace data);

    public LoadPlace deleteLoadPlace(Long id);

    public LoadPlace findLoadPlace(Long id);

    public PaginationResult<List<LoadPlace>> findByDate(String searchParam, String startDateParam, String endDateParam, int first, int last, String sortParam, String sortType);
}
