package transp.core.repositories;

import transp.core.entities.Client;
import transp.core.entities.Order;
import transp.rest.tools.PaginationResult;

import java.util.List;

public interface ClientRepo {

    public Client createClient(Client data);

    public Client updateClient(Long id, Client data);

    public Client deleteClient(Long id);

    public Client findClient(Long id);

    public PaginationResult<List<Order>> getOrderByClientId(long clientId, String searchParam, int first, int last, String startDateParam, String endDateParam, String sortParam, String sortType);

    public PaginationResult<List<Client>> getAllClient(int first, int last, String sortParam, String sortType);

    public PaginationResult<List<Client>> findAllClient(String searchParam, int first, int last, String sortParam, String sortType);

}
